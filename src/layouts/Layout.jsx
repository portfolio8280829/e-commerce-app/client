import { Box } from '@mui/material';
import React from 'react';
import { Outlet } from "react-router-dom";
import Header from '../components/Header';

const Layout = () => {
  return (
     <Box
        sx={{
           display: "grid",
           width: "100%",
           height: "100vh",
           gridTemplateRows: "120px 1fr",
        }}
     >
        <Box>
           <Header />
        </Box>
        <Box sx={{ gridRow: "span 1" }}>
           <Outlet />
        </Box>
     </Box>
  );
}

export default Layout
