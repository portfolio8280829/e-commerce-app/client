import { Box } from '@mui/material';
import React from 'react';
import { Outlet } from 'react-router-dom';
import Header from '../pages/User/components/Header';

const UserLayout = () => {

  return (
     <Box sx={{display:'grid', gridTemplateColumns: '80px, 1fr', width:'100%', height: '100%'}}>
      <Header />
         <Box gridRow='span 1'>
              <Outlet />
         </Box>
     </Box>
  );
}

export default UserLayout
