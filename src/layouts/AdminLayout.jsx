import React, { useState } from 'react'
import { GlobalState } from '../context/Context'
import { Box, useMediaQuery } from '@mui/material'
import Sidebar from '../components/Sidebar'
import { Outlet } from 'react-router-dom'

const AdminLayout = () => {
  const isNonMobile = useMediaQuery('(min-width:600px)')
  const [isSidebarOpen, setIsSidebarOpen] = useState(true)
  const {authState} = GlobalState()
  const user = authState.user;
  return (
    <Box display={isNonMobile ? "flex": 'block'} width= '100%' height='100%'>
      <Sidebar 
        user={user || {}}
        isNonMobile={isNonMobile}
        drawerWidth='250px'
        isSidebarOpen={isSidebarOpen}
        setIsSidebarOpen={setIsSidebarOpen}
      />
      <Box flexGrow={1}>
       <Outlet/>
      </Box>
    </Box>
   

  )
}

export default AdminLayout
