const AUTH_URL = "http://localhost:5000/api/v1/auth"
const USER_URL = "http://localhost:5000/api/v1/user"
const PRODUCT_URL = "http://localhost:5000/api/v1/product"
const REVIEW_URL = "http://localhost:5000/api/v1/review"
const ORDER_URL = "http://localhost:5000/api/v1/order"
const CART_URL = "http://localhost:5000/api/v1/cart"

export {
  AUTH_URL,
  USER_URL,
  PRODUCT_URL,
  REVIEW_URL,
  ORDER_URL,
  CART_URL
}