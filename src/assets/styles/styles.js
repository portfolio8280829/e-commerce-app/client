const FlexBetween = {
  display:'flex',
  alignItems:'center',
  justifyContent:'flex-between',
  width:'100%',
  gap:'.5rem'
}

const Label = { 
  padding:'1rem 2rem', 
  textAlign:'center'
}

const FormContainer = {
  display:'flex',
  alignItems:'center', 
  justifyContent:'center', 
  padding:'.5rem', 
  margin:'0.5rem 0'
}

 const Container = {
  width: '100%',
  margin: '0 auto',
  padding: '1rem'
}

const AbsCenter = {
  top: '50%',
  left: '50%',
  transform: 'translate(-50%,-50%)',
  position: 'absolute'
}


export {
  FlexBetween,
  Label,
  FormContainer,
  Container,
  AbsCenter
}