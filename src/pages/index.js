import ForgotPassword from "./Auth/ForgotPassword";
import Login from "./Auth/Login";
import Register from "./Auth/Register";
import ResetPassword from "./Auth/ResetPassword";
import VerifyEmail from "./Auth/VerifyEmail";
import AdminGuard from "./Guards/AdminGuard";
import UserGuard from "./Guards/UserGuard";
import Home from "./Home/Home";
import Notfound from "./error/Notfound";

// Admin Routes

import Breakdown from "./Admin/Breakdown";
import Customers from "./Admin/Customers";
import Dashboard from "./Admin/Dashboard";
import Orders from "./Admin/Orders";
import Overview from "./Admin/Overview";
import Products from "./Admin/Products";
import Reviews from "./Admin/Reviews";
import Settings from "./Admin/Settings";

// User Routes

import UserCart from "./User/UserCart";
import UserDashboard from "./User/UserDashboard";
import UserJobOrder from "./User/UserJobOrder";
import UserOrderItem from "./User/UserOrderItem";
import UserOrders from "./User/UserOrders";
import UserProducts from "./User/UserProducts";
import UserReviews from "./User/UserReviews";
import UserSettings from "./User/UserSettings";

export {
  AdminGuard, Breakdown, Customers, Dashboard, ForgotPassword, Home, Login, Notfound, Orders, Overview, Products, Register,
  ResetPassword, Reviews, Settings,
  UserCart,
  UserDashboard, UserGuard, UserJobOrder,
  UserOrderItem, UserOrders, UserProducts, UserReviews,
  UserSettings, VerifyEmail
};

