import DeleteIcon from "@mui/icons-material/Delete";
import { Box, Button, Card, CardActions, CardContent, CardMedia, Divider, IconButton, Link, Stack, TextField, Typography } from '@mui/material';
import React, { useState } from 'react';
import { GlobalState } from '../../context/Context';
import { Cart, Order } from "../../context/actions";
import { Link as RouterLink } from "react-router-dom";
                     import LockIcon from "@mui/icons-material/Lock";


const CartItem = ({ cartItem }) => {
   const { product, quantity, _id } = cartItem;
   const {cartDispatch} = GlobalState()
   const [cartQuantity, setCartQuantity] = useState(quantity)

    const cartItemTotal = product.price  * quantity;


   const handleDelete = async (id) => {
      await Cart.deleteFromCart(cartDispatch, id)
      await Cart.getUserCart(cartDispatch)
   };

   const handleChange = async (e, productId)=> {
      const regex = /^[1-9\b]+$/;
      setCartQuantity(e.target.value);
   
      if(e.target.value === "" ||  regex.test(e.target.value) || e.target.value === 0)
      {
         const payload = {
            quantity: Number(e.target.value),
            product: productId,
         };
         await Cart.addToCart(cartDispatch, payload);
         await Cart.getUserCart(cartDispatch)
         return e.target.value === 1
      }
   }
   return (
      <Card>
         <CardContent>
            <Stack
               direction="row"
               alignItems="start"
               justifyContent="space-between"
               spacing={2}
            >
               <CardMedia
                  component="img"
                  src={product.image.url}
                  alt={product.name}
                  height="200"
                  sx={{
                     objectFit: "cover/contain",
                     aspectRatio: "4/3",
                     background: "no-repeat center fixed",
                     width: "250px",
                     borderRadius: "5px",
                  }}
               />

               <Stack direction="column" width="100%">
                  <Typography sx={{ fontWeight: 600, marginBottom: "1rem" }}>
                     {product.name}
                  </Typography>
                  <Typography sx={{ textTransform: "capitalize" }}>
                     {product.category}
                  </Typography>
                  <Typography
                     sx={{ textTransform: "capitalize", marginBottom: "1rem" }}
                  >
                     {product.company}
                  </Typography>
                  <Typography sx={{ fontWeight: 600 }}>
                     {product.inventory} items left
                  </Typography>
                 
               </Stack>

               <Stack
                  direction="column"
                  spacing={1}
                  alignItems="left"
                  width="100%"
               >
                  <Typography>Each</Typography>
                  <Typography
                     sx={{
                        fontWeight: 600,
                        marginBottom: "1rem",
                        fontSize: "18px",
                     }}
                  >
                     ₱ {product.price.toFixed(2)}
                  </Typography>
               </Stack>

               <CardActions>
                  <Stack
                     direction="column"
                     spacing={1}
                     alignItems="center"
                     justifyContent="center"
                  >
                     <TextField
                        variant="outlined"
                        label="Quantity"
                        type="number"
                        value={cartQuantity}
                        inputProps={{ min: 1, max: Number(product.inventory) }}
                        sx={{ width: '12ch' }}
                        onChange={(e) => {
                           handleChange(e, product._id);
                        }}
                     />
                  </Stack>
               </CardActions>
               <Stack
                  direction="column"
                  spacing={1}
                  alignItems="end"
                  width="100%"
               >
                  <Typography sx={{
                        fontWeight: 600,
                        marginBottom: "1rem",
                        fontSize: "18px",
                        textAlign: "right",
                     }}>Total</Typography>
                  <Typography
                     sx={{
                        fontWeight: 600,
                        marginBottom: "1rem",
                        fontSize: "18px",
                        textAlign: "right",
                     }}
                  >
                     ₱ {cartItemTotal.toFixed(2)}
                  </Typography>
               </Stack>
               <CardActions>
                  <Stack direction="column" spacing={1}>
                     <IconButton onClick={() => handleDelete(_id)}>
                        <DeleteIcon />
                     </IconButton>
                  </Stack>
               </CardActions>
            </Stack>
         </CardContent>
      </Card>
   );
};


const UserCart = () => {
  const {cartState, cartDispatch, orderDispatch} = GlobalState()
  const {cart} = cartState


  const handleCreateOrder = async () => {
   const items = {
      items: cart,
      tax,
      shippingFee,
      subTotal,
      total,
   }

   await Order.createOrder(orderDispatch, items)
   await Cart.getUserCart(cartDispatch)
   await Order.getUserOrders(orderDispatch)
  };
  const subTotal = cart.reduce((acc, cart)=>{
    return acc + (cart.product.price * cart.quantity)
  }, 0)

  const shippingFee = 120
  const tax = (subTotal + 5) / 100;
  const total = subTotal + tax + shippingFee;

  console.log(cart)
  let content;

  if(cart.length > 0){
   content = (
      <>
         <Box
            sx={{
               height: "100%",
               display: "flex",
               flexDirection: "column",
               gap: "1rem",
            }}
            flexGrow={1}
         >
            <Typography sx={{ fontWeight: 600 }}>
               Cart {cart.length} Items
            </Typography>
            <Divider />
            {cart.map((cartItem,idx) => (
               <CartItem key={idx} cartItem={cartItem} />
            ))}
         </Box>
         <Box
            flexGrow={0.3}
            sx={{
               display: "flex",
               flexDirection: "column",
               padding: "1rem",
               alignItems: "end",
               width: "100%",
            }}
         >
            <Stack
               direction="row"
               alignItems="end"
               justifyContent="space-between"
               sx={{ width: "100%", padding: ".3rem 0" }}
            >
               <Typography sx={{ color: "#555", fontSize: "18px" }}>
                  Gross Total
               </Typography>
               <Typography sx={{ color: "#555", fontSize: "18px" }}>
                  ₱ {subTotal.toFixed(2)}
               </Typography>
            </Stack>
            <Stack
               direction="row"
               alignItems="start"
               justifyContent="space-between"
               sx={{ width: "100%", padding: ".3rem 0" }}
            >
               <Typography sx={{ color: "#555" }}>Shipping Cost</Typography>
               <Typography sx={{ color: "#555" }}>
                  ₱ {shippingFee.toFixed(2)}
               </Typography>
            </Stack>
            <Stack
               direction="row"
               alignItems="end"
               justifyContent="space-between"
               sx={{ width: "100%", padding: ".3rem 0" }}
            >
               <Typography sx={{ color: "#555" }}>Discount</Typography>
               <Typography sx={{ color: "#555" }}>- ₱0</Typography>
            </Stack>

            <Stack
               direction="row"
               alignItems="end"
               justifyContent="space-between"
               sx={{ width: "100%", padding: ".3rem 0" }}
            >
               <Typography sx={{ color: "#555" }}>Tax (5%)</Typography>
               <Typography sx={{ color: "#555" }}>
                  ₱ {tax.toFixed(2)}
               </Typography>
            </Stack>

            <Stack
               direction="row"
               alignItems="end"
               justifyContent="space-between"
               sx={{ width: "100%", padding: ".3rem 0" }}
            >
               <Typography sx={{ fontSize: "18px", fontWeight: 600 }}>
                  Total
               </Typography>
               <Typography sx={{ fontSize: "18px", fontWeight: 600 }}>
                  ₱ {total.toFixed(2)}
               </Typography>
            </Stack>
            <Stack
               direction="row"
               alignItems="end"
               justifyContent="end"
               sx={{ width: "100%", padding: "1rem 0" }}
            >
               <Button
                  variant="contained"
                  sx={{ padding: "1rem 2.5rem" }}
                  onClick={handleCreateOrder}
               >
                  <LockIcon /> Checkout
               </Button>
            </Stack>
         </Box>
      </>
   );
  }
  else{
   content = (
      <>
       <Box sx={{display:'flex',flexDirection:'column',gap:'1rem', alignItems:"center" ,justifyContent:'center', width: '100%', height: '90vh'}}>
         <Typography variant='h4'>
            Your cart is empty
         </Typography>
            <Button variant='contained' component={RouterLink} to="/shop/">Back to shop</Button>
       </Box>
      </>
   );

  }
  return (
     <Box
        sx={{
           width: "100%",
           height: "100%",
           display: "flex",
           alignItems: "center",
           justifyContent: "center",
           padding: "1rem",
        }}
     >
        <Box
           sx={{
              height: "100%",
              width: "100%",
              maxWidth: "1400px",
              display: "flex",
              flexWrap:'wrap'
           }}
        >
           {content}
        </Box>
     </Box>
  );
}

export default UserCart
