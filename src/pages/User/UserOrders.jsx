import { Box, Button, Card, CardContent, CardMedia, Divider, Stack, Tab, Tabs, Typography } from '@mui/material';
import dayjs from 'dayjs';
import React from 'react';
import { GlobalState } from '../../context/Context';
function CustomTabPanel(props) {
   const { children, value, index, ...other } = props;

   return (
      <Box
         role="tabpanel"
         hidden={value !== index}
         id={`simple-tabpanel-${index}`}
         aria-labelledby={`simple-tab-${index}`}
         {...other}
      >
         {value === index && (
            <Box sx={{ p: 3 }}>
               <Box>{children}</Box>
            </Box>
         )}
      </Box>
   );
}

function a11yProps(index) {
   return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
   };
}


const SingleOrder = (order) => {
  console.log(order.orderItem)
  const {shippingFee, subTotal, total,status, tax, createdAt, orderItems} = order.orderItem
  return (
     <Card
        sx={{
           backgroundImage: "none",
           borderRadius: "0.55rem",
           width: "100%",
           transition: ".125s ease-in",
           "&:hover": {
              cursor: "pointer",
              boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",
           },
        }}
     >
        <CardContent
           sx={{ display: "flex", flexDirection: "column", gap: "1rem" }}
        >
           <Stack
              direction="row"
              justifyContent="space-between"
              alignItems="start"
           >
              <Stack direction="column" spacing={2}>
                 <Typography>ORDER PLACED</Typography>
                 <Typography>
                    {dayjs(createdAt).format("MMMM DD, YYYY HH:mm a")}
                 </Typography>
              </Stack>
              <Stack direction="column" spacing={2}>
                 <Typography>TOTAL</Typography>
                 <Typography>₱ {Number(total).toFixed(2)}</Typography>
              </Stack>

              <Stack direction="column" spacing={2}>
                 <Typography>ORDER DETAILS</Typography>
                 <Typography sx={{ textTransform: "capitalize" }}>
                    Status: {status}
                 </Typography>
              </Stack>
           </Stack>
           <Stack
              direction="row"
              spacing={2}
              padding="1rem"
              justifyContent="space-between"
           >
              <CardMedia
                 height="200"
                 component="img"
                 src={orderItems[0].image}
                 alt={orderItems[0].name}
                 sx={{
                    width: "200px",
                    objectFit: "cover/contain",
                    aspectRatio: "4/3",
                    background: "no-repeat center fixed",
                    borderRadius: "5px",
                 }}
              />
              <Stack flexGrow={1}>
                 <Typography sx={{ mb: "1.5rem" }}>
                    {orderItems[0].name}
                 </Typography>
                 <Typography sx={{ mb: "1.5rem" }}>
                    ₱ {orderItems[0].price.toFixed(2)}
                 </Typography>
              </Stack>
              <Stack direction="column" spacing={2}>
                 <Button
                    sx={{ textTransform: "capitalize" }}
                    variant="contained"
                 >
                    Buy Again
                 </Button>
                 <Button
                    sx={{ textTransform: "capitalize" }}
                    variant="contained"
                 >
                    View More
                 </Button>
              </Stack>
           </Stack>
           <Divider />
        </CardContent>
     </Card>
  );
}

const UserOrders = () => {
  const {orderState} = GlobalState()
  const {orders} = orderState

    const [value, setValue] = React.useState(0);

    const pendingOrders = orders.filter((order)=> order.status === 'pending')
    const approvedOrders = orders.filter((order)=> order.status === 'approved')
    const completedOrders = orders.filter((order)=> order.status === 'completed')
    

    console.log(pendingOrders)

    const handleChange = (event, newValue) => {
       setValue(newValue);
    };


  return (
     <Box sx={{ width: "100%" }}>
        <Box
           sx={{
              borderBottom: 1,
              borderColor: "divider",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
           }}
        >
           <Tabs
              value={value}
              onChange={handleChange}
              aria-label="basic tabs example"
           >
              <Tab label="Pending" {...a11yProps(0)} />
              <Tab label="Approved" {...a11yProps(1)} />
              <Tab label="Delivered" {...a11yProps(2)} />
           </Tabs>
        </Box>
        <CustomTabPanel value={value} index={0}>
           <Box
              sx={{
                 width: "100%",
                 height: "100%",
                 display: "flex",
                 alignItems: "center",
                 justifyContent: "center",
                 padding: "1rem",
              }}
           >
              <Stack
                 direction="column"
                 spacing={1}
                 sx={{
                    height: "100%",
                    width: "100%",
                    maxWidth: "1400px",
                    display: "flex",
                    flexWrap: "wrap",
                 }}
              >
                 {pendingOrders &&
                    pendingOrders.map((order, idx) => (
                       <SingleOrder key={idx} orderItem={order} />
                    ))}
              </Stack>
           </Box>
        </CustomTabPanel>
        <CustomTabPanel value={value} index={1}></CustomTabPanel>
        <CustomTabPanel value={value} index={2}></CustomTabPanel>
     </Box>
  );
}

export default UserOrders
 