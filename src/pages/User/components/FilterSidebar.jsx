import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import SearchIcon from "@mui/icons-material/Search";
import { Accordion, AccordionDetails, AccordionSummary, Box, Chip, Fab, FormControl, FormControlLabel, InputBase, ListItemButton, Radio, RadioGroup, Rating, Stack, Typography, alpha, styled } from "@mui/material";
import React from "react";
import { GlobalState } from '../../../context/Context';
import { product_filter } from '../../../context/constants';

 const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: "inherit",
    "& .MuiInputBase-input": {
       padding: theme.spacing(1, 1, 1, 0),
       // vertical padding + font size from searchIcon
       paddingLeft: `calc(1em + ${theme.spacing(4)})`,
       transition: theme.transitions.create("width"),
       width: "100%",
       [theme.breakpoints.up("md")]: {
          width: "20ch",
       },
    },
 }));


 const Search = styled("div")(({ theme }) => ({
    position: "relative",
    boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",
    borderRadius: "5px",
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
       marginLeft: theme.spacing(3),
       width: "auto",
    },
 }));

 const SearchIconWrapper = styled("div")(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
 }));

const FilterSidebar = ({ isNonMobile, drawerWidth, products }) => {

  const {productFilters, productFiltersDispatch} = GlobalState()

  const maxPrice = Math.max(...products.map((product) => product.price));
  const categories = Object.values(products.reduce((a,{category})=>{
      a[category]={category}
      return a
  },{}))
  const companies = Object.values(products.reduce((a,{company})=>{
      a[company] = { company };
      return a
  },{}))
  const colors = Object.values(
     products.reduce((a, { colors }) => {
        a[colors] = { colors };
        return a;
     }, {})
  );



  const sortItem = [
     {
        text: "Top Sales",
        action: product_filter.SORT_BY_SALES,
     },
     {
        text: "New Arrivals",
        action: product_filter.SORT_BY_DATE,
     },
     {
        text: "Featured",
        action: product_filter.SORT_BY_FEATURED
     },
     {
        text: "Free Shipping",
        action: product_filter.SORT_BY_SHIPPING
     },
  ];

  const priceItem = [
     {
        text: "PHP 0 - PHP 1000",
        value: 1,
     },
     {
        text: "PHP 1000 - PHP 2000",
        value:2,
     },
     {
        text: "PHP 2000 - PHP 3000",
        value: 3
     },
     {
        text: "PHP 4000 - PHP 5000",
        value: 4
     },
     {
        text: "PHP > 5000",
        value: 5
     },
  ];

  const ratingItem = [
    {
      value: 5,
    },
    {
      value: 4
    },
    {
      value: 3
    },
    {
      value: 2
    },
    {
      value: 1
    }
  ]

  const stopPropagation = (e) => {
     e.stopPropagation();
     
  };
 
  const activeFilters = () =>{
   const keys = Object.keys(productFilters)
   let filtered = keys.filter((key) => {
      return productFilters[key];
   });
   return filtered
  }

  const MuiAccordion = styled(Accordion)((({theme})=>{
    return {
       boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",
       borderRadius: '5px',
       border:'none'
    };
  }))
  
   return (
      <Box sx={{ width: drawerWidth, padding: "1rem", height: "100%" }}>
         <Stack direction="column" spacing={2}>
            <Typography>Filters</Typography>
            <Stack direction="row" spacing={0.5} flexWrap="wrap" useFlexGap>
               {activeFilters().map((filter) => {
                  return <Chip label={filter} variant="outlined" />;
               })}
            </Stack>
            <Chip
               label="Clear Filters"
               variant="contained"
               onClick={(e) => {
                  e.stopPropagation();
                  productFiltersDispatch({
                     type: product_filter.CLEAR_FILTERS,
                  });
               }}
            />
            <Search>
               <SearchIconWrapper>
                  <SearchIcon />
               </SearchIconWrapper>
               <StyledInputBase
                  placeholder="Search…"
                  inputProps={{ "aria-label": "search" }}
                  onChange={(e) => {
                     e.stopPropagation();
                     productFiltersDispatch({
                        type: product_filter.FILTER_BY_SEARCH,
                        payload: e.target.value,
                     });
                  }}
               />
            </Search>
            <MuiAccordion
               elevation={0}
               sx={{ "&:before": { display: "none" } }}
            >
               <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
               >
                  Sort
               </AccordionSummary>
               <AccordionDetails>
                  {sortItem.map((item) => (
                     <ListItemButton
                        key={item.text}
                        onClick={(e) => {
                           e.stopPropagation();
                           productFiltersDispatch({ type: item.action });
                        }}
                     >
                        {item.text}
                     </ListItemButton>
                  ))}
               </AccordionDetails>
            </MuiAccordion>
            <MuiAccordion
               elevation={0}
               sx={{ "&:before": { display: "none" } }}
            >
               <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
               >
                  <Typography>Price</Typography>
               </AccordionSummary>
               <AccordionDetails>
                  <FormControl>
                     <RadioGroup
                        aria-labelledby="demo-radio-buttons-group-label"
                        defaultValue={productFilters.Price}
                        name="radio-buttons-group"
                     >
                        {priceItem.map((item) => (
                           <FormControlLabel
                              key={item.value}
                              value={item.value}
                              control={<Radio />}
                              label={item.text}
                              onChange={(e) => {
                                 e.stopPropagation();
                                 productFiltersDispatch({
                                    type: product_filter.SORT_BY_PRICE,
                                    payload: Number(item.value),
                                 });
                              }}
                           />
                        ))}
                     </RadioGroup>
                  </FormControl>
               </AccordionDetails>
            </MuiAccordion>

            <MuiAccordion
               elevation={0}
               sx={{ "&:before": { display: "none" } }}
            >
               <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
               >
                  <Typography>Category</Typography>
               </AccordionSummary>
               <AccordionDetails>
                  {categories.map((category) => (
                     <ListItemButton
                        key={category.category}
                        onClick={(e) => {
                           e.stopPropagation();
                           productFiltersDispatch({
                              type: product_filter.SORT_BY_CATEGORY,
                              payload: category.category,
                           });
                        }}
                        sx={{ textTransform: "capitalize" }}
                     >
                        {category.category}
                     </ListItemButton>
                  ))}
               </AccordionDetails>
            </MuiAccordion>

            <MuiAccordion
               elevation={0}
               sx={{ "&:before": { display: "none" } }}
            >
               <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
               >
                  <Typography>Company</Typography>
               </AccordionSummary>
               <AccordionDetails>
                  {companies.map((company) => (
                     <ListItemButton
                        key={company.company}
                        onClick={(e) => {
                           e.stopPropagation();
                           productFiltersDispatch({
                              type: product_filter.SORT_BY_COMPANY,
                              payload: company.company,
                           });
                        }}
                        sx={{ textTransform: "capitalize" }}
                     >
                        {company.company}
                     </ListItemButton>
                  ))}
               </AccordionDetails>
            </MuiAccordion>

            <MuiAccordion
               elevation={0}
               sx={{ "&:before": { display: "none" } }}
            >
               <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
               >
                  <Typography>Rating</Typography>
               </AccordionSummary>
               <AccordionDetails>
                  <FormControl>
                     <RadioGroup
                        aria-labelledby="demo-radio-buttons-group-label"
                        defaultValue={productFilters.Rating}
                        name="radio-buttons-group"
                     >
                        {ratingItem.map((item) => (
                           <Stack
                              direction="row"
                              key={item.value}
                              alignItems="center"
                           >
                              <FormControlLabel
                                 value={item.value}
                                 control={<Radio />}
                                 onClick={(e) => stopPropagation(e)}
                                 onChange={(e) => {
                                    e.stopPropagation();
                                    productFiltersDispatch({
                                       type: product_filter.SORT_BY_RATING,
                                       payload: Number(item.value),
                                    });
                                 }}
                              />
                              <Rating value={item.value} readOnly />
                           </Stack>
                        ))}
                     </RadioGroup>
                  </FormControl>
               </AccordionDetails>
            </MuiAccordion>
            <MuiAccordion
               elevation={0}
               sx={{ "&:before": { display: "none" } }}
            >
               <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
               >
                  <Typography>Colors</Typography>
               </AccordionSummary>
               <AccordionDetails>
                  <Stack direction="row" spacing={0.5}>
                     {colors.map((color) => (
                        <Fab
                           key={color.colors}
                           size="small"
                           sx={{ backgroundColor: color.colors }}
                        ></Fab>
                     ))}
                  </Stack>
               </AccordionDetails>
            </MuiAccordion>
         </Stack>
      </Box>
   );
};

export default FilterSidebar;
