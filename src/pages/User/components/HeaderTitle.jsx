import { Box, Typography } from '@mui/material'
import React from 'react'

const HeaderTitle = ({title, subtitle}) => {
  return (
     <Box>
        <Typography
           variant="h2"
           fontWeight="bold"
           sx={{ mb: "5px", fontSize:'24px' }}
        >
           {title}
        </Typography>
        <Typography variant="h5" sx={{fontSize:'16px'}}>
           {subtitle}
        </Typography>
     </Box>
  );
}

export default HeaderTitle
