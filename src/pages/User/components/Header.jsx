import AccountCircle from '@mui/icons-material/AccountCircle';
import BallotIcon from "@mui/icons-material/Ballot";
import MenuIcon from '@mui/icons-material/Menu';
import MoreIcon from '@mui/icons-material/MoreVert';
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import AppBar from '@mui/material/AppBar';
import Badge from '@mui/material/Badge';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import InputBase from '@mui/material/InputBase';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { alpha, styled } from '@mui/material/styles';
import { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { Auth } from '../../../context/actions';
import { GlobalState } from '../../../context/Context';
import LocalMallIcon from "@mui/icons-material/LocalMall";

const Header = () => {
  const {authState, cartState,authDispatch} = GlobalState()
  const cart = cartState.cart.length;
  const navigate =useNavigate()
    const [anchorEl, setAnchorEl] = useState(null);
    const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);

    const isMenuOpen = Boolean(anchorEl);
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

    const handleProfileMenuOpen = (event) => {
       setAnchorEl(event.currentTarget);
    };

    const handleMobileMenuClose = () => {
       setMobileMoreAnchorEl(null);
    };

    const handleMenuClose = () => {
       setAnchorEl(null);
       handleMobileMenuClose();
    };

    const handleMobileMenuOpen = (event) => {
       setMobileMoreAnchorEl(event.currentTarget);
    };


    const menuId = "primary-search-account-menu";
    const renderMenu = (
       <Menu
          anchorEl={anchorEl}
          anchorOrigin={{
             vertical: "top",
             horizontal: "right",
          }}
          id={menuId}
          keepMounted
          transformOrigin={{
             vertical: "top",
             horizontal: "right",
          }}
          open={isMenuOpen}
          onClose={handleMenuClose}
       >
          <MenuItem
             component={Link}
             to="/shop/settings"
             onClick={handleMenuClose}
          >
             User Settings
          </MenuItem>
          <MenuItem
             onClick={async () => {
                await Auth.logout(authDispatch);
                navigate("/");
                handleMenuClose();
                window.location.reload();
             }}
          >
             Logout
          </MenuItem>
       </Menu>
    );

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
     <Menu
        anchorEl={mobileMoreAnchorEl}
        anchorOrigin={{
           vertical: "top",
           horizontal: "right",
        }}
        id={mobileMenuId}
        keepMounted
        transformOrigin={{
           vertical: "top",
           horizontal: "right",
        }}
        open={isMobileMenuOpen}
        onClose={handleMobileMenuClose}
     >
        <MenuItem component={Link} to="/shop/">
           <IconButton size="large" color="inherit">
              <LocalMallIcon />
           </IconButton>
           <p>Shop</p>
        </MenuItem>
        <MenuItem component={Link} to="/shop/cart">
           <IconButton
              size="large"
              aria-label="show cart items"
              color="inherit"
           >
              <Badge badgeContent={cart} color="error">
                 <ShoppingCartIcon />
              </Badge>
           </IconButton>
           <p>Cart</p>
        </MenuItem>
        <MenuItem component={Link} to="/shop/orders">
           <IconButton
              size="large"
              aria-label="orders"
              color="inherit"
           >
                 <BallotIcon />
           </IconButton>
           <p>Orders</p>
        </MenuItem>
        <MenuItem onClick={handleProfileMenuOpen}>
           <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="primary-search-account-menu"
              aria-haspopup="true"
              color="inherit"
           >
              <AccountCircle />
           </IconButton>
           <Link>Profile</Link>
        </MenuItem>
     </Menu>
  );
 
  return (
     <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
           <Toolbar>
              <Typography
                 variant="h6"
                 noWrap
                 component={Link}
                 to="/shop/"
                 sx={{ display: { xs: "none", sm: "block" } }}
              >
                 E-Commerce Website
              </Typography>
             
              <Box sx={{ flexGrow: 1 }} />

              <Box sx={{ display: { xs: "none", md: "flex" } }}>
                 <IconButton
                    size="large"
                    color="inherit"
                    component={Link}
                    to="/shop/"
                 >
                       <LocalMallIcon />
                 </IconButton>
                 <IconButton
                    size="large"
                    aria-label="show cart items"
                    color="inherit"
                    component={Link}
                    to="/shop/cart"
                 >
                    <Badge badgeContent={cart} color="error">
                       <ShoppingCartIcon />
                    </Badge>
                 </IconButton>
                 <IconButton
                    size="large"
                    aria-label="orders"
                    color="inherit"
                    component={Link}
                    to="/shop/orders"
                 >
                       <BallotIcon />
                 </IconButton>
                 <IconButton
                    size="large"
                    edge="end"
                    aria-label="account of current user"
                    aria-controls={menuId}
                    aria-haspopup="true"
                    onClick={handleProfileMenuOpen}
                    color="inherit"
                 >
                    <AccountCircle />
                 </IconButton>
              </Box>
              <Box sx={{ display: { xs: "flex", md: "none" } }}>
                 <IconButton
                    size="large"
                    aria-label="show more"
                    aria-controls={mobileMenuId}
                    aria-haspopup="true"
                    onClick={handleMobileMenuOpen}
                    color="inherit"
                 >
                    <MoreIcon />
                 </IconButton>
              </Box>
           </Toolbar>
        </AppBar>
        {renderMobileMenu}
        {renderMenu}
     </Box>
  );
}

export default Header
