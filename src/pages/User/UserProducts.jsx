// icons

import LocalShippingIcon from "@mui/icons-material/LocalShipping";
import LocalShippingOutlinedIcon from "@mui/icons-material/LocalShippingOutlined";
import StarIcon from "@mui/icons-material/Star";
import StarBorderOutlined from "@mui/icons-material/StarBorderOutlined";

// 
import { Box, Button, Card, CardActions, CardContent, CardMedia, Chip, Rating, Skeleton, Stack, Tooltip, Typography, useMediaQuery } from '@mui/material';
import React, { useState } from 'react';
import { GlobalState } from '../../context/Context';
import FilterSidebar from './components/FilterSidebar';
import HeaderTitle from './components/HeaderTitle';
import { Cart } from "../../context/actions";
import { product, product_filter } from "../../context/constants";

const SingleProduct = ({
   _id,
   name,
   category,
   company,
   inventory,
   price,
   featured,
   averageRating,
   freeShipping,
   description,
   image,
   slug,
   numOfReviews
}) => {
   const { cartDispatch } = GlobalState();

   return (
      <Card
         sx={{
            backgroundImage: "none",
            borderRadius: "0.55rem",
            width: "100%",
            transition:'.125s ease-in',
            "&:hover": {
               cursor: "pointer",
               boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",
            },
         }}
      >
         <Stack direction="column" justifyContent="space-between">
            <CardMedia
               height="200"
               component="img"
               src={image.url}
               alt={name}
               sx={{
                  objectFit: "cover/contain",
                  aspectRatio: "4/3",
                  background: "no-repeat center fixed",
               }}
            />
            <CardContent>
               <Typography
                  sx={{ fontSize: 14 }}
                  gutterBottom
                  textTransform="capitalize"
               >
                  {category}
               </Typography>
               <Typography variant="h5" component="div">
                  {name}
               </Typography>
               <Typography sx={{ mb: "1.5rem" }}>
                  ₱ {Number(price).toFixed(2)}
               </Typography>
               <Stack direction="row" alignItems="end" spacing={0.5}>
                  <Rating value={averageRating} readOnly />
                  <Typography sx={{ color: "gray", fontSize: "12px" }}>
                     ({numOfReviews})
                  </Typography>
               </Stack>
               <Stack>
                  <Typography textTransform="capitalize">{company}</Typography>
                  <Typography variant="body2">{description}</Typography>
               </Stack>
            </CardContent>
         </Stack>
         <CardContent
            sx={{ display: "flex", flexDirection: "column", gap: ".5rem" }}
         >
            <Stack direction="row" spacing={2} justifyContent="space-between">
               <Typography sx={{ fontWeight: 600 }}>
                  {inventory} items left
               </Typography>
               <Stack direction="row" spacing={1.5}>
                  <Tooltip title="Featured">
                     <Typography>
                        {featured ? <StarIcon /> : <StarBorderOutlined />}
                     </Typography>
                  </Tooltip>
                  <Tooltip title="Free Shipping">
                     <Typography>
                        {freeShipping ? (
                           <LocalShippingIcon />
                        ) : (
                           <LocalShippingOutlinedIcon />
                        )}
                     </Typography>
                  </Tooltip>
               </Stack>
            </Stack>
         </CardContent>
         <CardActions>
            <Button sx={{ width: "100%", fontWeight: 600 }} variant="contained" onClick={async()=> {
               const payload = {product: _id, quantity: 1}
               await Cart.addToCart(cartDispatch, payload)
               await Cart.getUserCart(cartDispatch)
            }}>
               Add To Cart
            </Button>
         </CardActions>
      </Card>
   );
};

const UserProducts = () => {
  const isNonMobile = useMediaQuery('(min-width:600px)')
  const { authState, productState:{products, isLoading}, productFilters: {Rating, Featured, Sales, FreeShipping, Price, Category, Color, Date, Search, Company}, productFiltersDispatch} =
     GlobalState();
  const user = authState.user;

  const filteredProducts = () => {
   let sortedProducts = products

   if(Rating){
      sortedProducts = sortedProducts.filter((product)=> product.averageRating >= Rating)
   }
   if (Featured) {
      sortedProducts = sortedProducts.filter((product) => product.featured);
   }
   if(FreeShipping){
      sortedProducts = sortedProducts.filter((product)=> product.freeShipping)
   }
   if(Category){
      sortedProducts = sortedProducts.filter((product) =>
         product.category.includes(Category)
      );
   }
   if (Sales) {
      sortedProducts = sortedProducts.sort(
         (a, b) => b.numOfReviews - a.numOfReviews
      );
   }
   if(Color){
       sortedProducts = sortedProducts.filter((product) =>
          product.category.toLowerCase.includes(Color)
       );
   }
   if(Date){
       sortedProducts = sortedProducts.sort((a,b) => b.createdAt.localeCompare(a.createdAt)  )
   }
   if(Price){
      if(Price === 1) {
         sortedProducts = sortedProducts.filter((product)=> product.price <= 1000)
      }
      else if (Price === 2) {
         sortedProducts = sortedProducts.filter(
            (product) => product.price >= 1000 && product.price <= 2000
         );
      } else if (Price === 3) {
         sortedProducts = sortedProducts.filter(
            (product) => product.price >= 2000 && product.price <= 3000
         );
      } else if (Price === 4) {
         sortedProducts = sortedProducts.filter(
            (product) => product.price >= 4000 && product.price <= 5000
         );
      } else if (Price === 5) {
         sortedProducts = sortedProducts.filter(
            (product) => product.price >= 5000
         );
      }
   }
   if(Color){
      sortedProducts = sortedProducts.filter((product)=> product.color.includes(Color))
   }
   if(Company){
      sortedProducts = sortedProducts.filter(
         (product) => product.company === Company
      );
   }
   if (Search) {
      sortedProducts = sortedProducts.filter(
         (product) =>
            product.name.toLowerCase().includes(Search.toLowerCase()) ||
            product.category.toLowerCase().includes(Search.toLowerCase()) ||
            product.company.toLowerCase().includes(Search.toLowerCase())
      ); 
   }

   return sortedProducts
  }

  return (
     <Box
        sx={{
           display: "flex",
           alignItems: "start",
           justifyContent: "center",
           width: "100%",
           height: "100%",
        }}
     >
        <FilterSidebar
           products={products || {}}
           isNonMobile={isNonMobile}
           drawerWidth="300px"
        />
        <Box flexGrow={1}>
           <Box m="1.5rem 2.5rem">
              <HeaderTitle title="PRODUCTS" subtitle="List of products" />
              {products || isLoading ? (
                 <Box
                    mt="20px"
                    display="grid"
                    gridTemplateColumns="repeat(auto-fill, minmax(320px, 1fr))"
                    alignItems="start"
                    rowGap="20px"
                    columnGap="1.33%"
                    sx={{
                       "& > div": {
                          gridColumn: isNonMobile ? undefined : "span 4",
                       },
                    }}
                 >
                    {filteredProducts().length > 0 ? (
                       filteredProducts().map((product) => (
                          <SingleProduct
                             key={product._id}
                             _id={product._id}
                             name={product.name}
                             image={product.image}
                             category={product.category}
                             company={product.company}
                             rating={product.averageRating}
                             featured={product.featured}
                             freeShipping={product.freeShipping}
                             price={product.price}
                             inventory={product.inventory}
                             description={product.description}
                             slug={product.slug}
                             averageRating={product.averageRating}
                             numOfReviews={product.numOfReviews}
                          />
                       ))
                    ) : (
                       <Stack>
                          <Typography>No products found</Typography>
                          <Chip
                             label="Clear Filters"
                             variant="contained"
                             onClick={() =>
                                productFiltersDispatch({
                                   type: product_filter.CLEAR_FILTERS,
                                })
                             }
                          />
                      
                       </Stack>
                    )}
                 </Box>
              ) : (
                 <Skeleton animation="wave" variant="rectangular"></Skeleton>
              )}
           </Box>
        </Box>
     </Box>
  );
}

export default UserProducts
