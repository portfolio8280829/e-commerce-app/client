import React from "react";
import { Navigate } from "react-router-dom";
import { GlobalState } from "../../context/Context";

const AdminGuard = ({ children }) => {
   const { authState } = GlobalState();
   if (!authState.user.role === "admin") {
      window.location.reload()
      return <Navigate to="/" replace />;
   }

   return children;
};

export default AdminGuard;
