import React from "react";
import { Navigate } from "react-router-dom";
import { GlobalState } from "../../context/Context";

const UserGuard = ({ children }) => {
   const { authState } = GlobalState();
   if (!authState.user.role === "user") {
       window.location.reload();
      return <Navigate to="/" replace />;
   }

   return children;
};

export default UserGuard;
