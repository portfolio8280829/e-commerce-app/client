import React from 'react'
import {Box, LinearProgress} from '@mui/material'
import { User } from '../../context/actions'
import Header from './components/Header'
import { DataGrid } from "@mui/x-data-grid";
import { GlobalState } from '../../context/Context'
import VerifiedIcon from '@mui/icons-material/Verified';
import DoNotDisturbIcon from '@mui/icons-material/DoNotDisturb';
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import PersonIcon from "@mui/icons-material/Person";

const Customers = () => {
  const {userState, userDispatch} = GlobalState()
  const {users, isLoading} = userState
  console.log(users)

  const columns = [
     {
        field: "_id",
        headerName: "ID",
        flex: 1,
     },
     {
        field: "email",
        headerName: "Email",
        flex: 1,
     },
     {
        field: "username",
        headerName: "Username",
        flex: 1,
     },
     {
        field: "isVerified",
        headerName: "Verified",
        flex: 0.2,
        renderCell: (params) => {
           console.log(params.value);
           return params.value ? <VerifiedIcon /> : <DoNotDisturbIcon />;
        },
     },
     {
        field: "role",
        headerName: "Role",
        flex: 0.2,
        renderCell: (params) => {
           console.log(params.value);
           return params.value === "admin" ? (
              <ManageAccountsIcon />
           ) : (
              <PersonIcon />
           );
        },
     },
  ];

  return (
     <Box m="1.5rem 2.5rem">
        <Header title="USERS" subtitle="List of Users" />
        <Box
           mt="40px"
           height="75vh"
           sx={{
              "& .MuiDataGrid-root": {
                 border: "none",
              },
              "& .MuiDataGrid-cell": {
                 borderBottom: "none",
              },
              "& .MuiDataGrid-columnHeaders": {
                 borderBottom: "none",
              },
              "& .MuiDataGrid-virtualScroller": {},
              "& .MuiDataGrid-footerContainer": {
                 borderTop: "none",
              },
              "& .MuiDataGrid-toolbarContainer .MuiButton-text": {},
           }}
        >
           <DataGrid
              slots={{
                 loadingOverlay: LinearProgress,
              }}
              loading={isLoading || !users}
              getRowId={(row) => row._id}
              rows={users || []}
              columns={columns}
           />
        </Box>
     </Box>
  );
}

export default Customers
