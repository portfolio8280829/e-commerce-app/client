import React from "react";
import { Box, Button, Chip, LinearProgress } from "@mui/material";
import { Order } from "../../context/actions";
import Header from "./components/Header";
import { DataGrid } from "@mui/x-data-grid";
import { GlobalState } from "../../context/Context";

const renderApproveButton = () =>{
  return <Chip label="Approve Order" variant="contained" onClick={approveOrder} />;
}

const approveOrder = ()=>{

} 
const Orders = () => {
  
  const {orderState, orderDispatch} = GlobalState()
  const {orders, isLoading} = orderState

   const columns = [
      {
         field: "_id",
         headerName: "Order ID",
         flex: 0.5,
      },
      {
         field: "user",
         headerName: "User ID",
         flex: 0.5,
      },
      {
         field: "shippingFee",
         align: "right",
         headerName: "Shipping Fee",
         headerAlign: "right",
         valueFormatter: (params) => {
            if (params.value === null) {
               return "";
            }
            return `₱ ${params.value.toLocaleString()}`;
         },
         flex: 0.4,
      },
      {
         field: "tax",
         align: "right",
         headerName: "Tax",
         headerAlign: "right",
         valueFormatter: (params) => {
            if (params.value === null) {
               return "";
            }
            return `₱ ${params.value.toLocaleString()}`;
         },
         flex: 0.4,
      },
      {
         field: "subTotal",
         align: "right",
         headerName: "Gross Total",
         headerAlign: "right",
         valueFormatter: (params) => {
            if (params.value === null) {
               return "";
            }
            return `₱ ${params.value.toLocaleString()}`;
         },
         flex: 0.4,
      },
      {
         field: "total",
         align: "right",
         headerName: "Total",
         headerAlign: "right",
         valueFormatter: (params) => {
            if (params.value === null) {
               return "";
            }
            return `₱ ${params.value.toLocaleString()}`;
         },
         flex: 0.4,
      },
      {
         field: "status",
         align: "center",
         headerName: "Status",
         headerAlign:'center',
         flex: 0.4,
         renderCell: (params) => {
            return (
               <Chip
                  sx={{ textTransform: "capitalize" }}
                  label={params.value}
               />
            );
         },
      },
      {
         field: "action",
         headerName: "",
         flex: 0.3,
         renderCell: renderApproveButton,
      },
   ];

   return (
      <Box m="1.5rem 2.5rem">
         <Header title="USERS" subtitle="List of Users" />
         <Box
            mt="40px"
            height="75vh"
            sx={{
               "& .MuiDataGrid-root": {
                  border: "none",
               },
               "& .MuiDataGrid-cell": {
                  borderBottom: "none",
               },
               "& .MuiDataGrid-columnHeaders": {
                  borderBottom: "none",
               },
               "& .MuiDataGrid-virtualScroller": {},
               "& .MuiDataGrid-footerContainer": {
                  borderTop: "none",
               },
               "& .MuiDataGrid-toolbarContainer .MuiButton-text": {},
            }}
         >
            <DataGrid
               slots={{
                  loadingOverlay: LinearProgress,
               }}
               loading={isLoading || !orders}
               getRowId={(row) => row._id}
               rows={orders || []}
               columns={columns}
            />
         </Box>
      </Box>
   );
};

export default Orders;
