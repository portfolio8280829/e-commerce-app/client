import React from "react";
import { Box, Card, CardContent, Typography, LinearProgress, Rating } from "@mui/material";
import { Order } from "../../context/actions";
import Header from "./components/Header";
import { DataGrid } from "@mui/x-data-grid";
import { GlobalState } from "../../context/Context";

const renderRating = (rating) => {
   return <Rating readOnly value={rating.value} />
}

const renderProduct = (product) =>{
  const {value} = product

  
  return (
     value ? 
        <Box sx={{padding:'.5rem 0', display:'flex', justifyContent: 'center', flexDirection:'column'}}>
           <Typography
              sx={{ fontSize: 14 }}
              gutterBottom
              textTransform="capitalize"
           >
              {value._id}
           </Typography>
           <Typography variant="h6" component="div">
              {value.name}
           </Typography>
           <Typography sx={{ mb: "1.5rem" }}>
              ₱ {Number(value.price).toFixed(2)}
           </Typography>
           <Typography textTransform="capitalize">{value.category}</Typography>
        </Box>
     :
     null
  );
}

const Reviews = () => {
   const { reviewState, reviewDispatch } = GlobalState();
   const { reviews, isLoading } = reviewState;

   const columns = [
      {
         field: "_id",
         headerName: "Order ID",
         flex: 1,
      },
      {
         field: "user",
         headerName: "User ID",
         flex: 1,
      },
      {
         field: "product",
         headerName: "Product Item",
         flex: 1,
         renderCell: renderProduct,
      },
      {
         field: "title",
         headerName: "Title",
         flex: 0.5,
      },
      {
         field: "comment",
         headerName: "Comment",
         flex: 1,
      },
      {
         field: "rating",
         headerName: "Rating",
         flex: .5,
         renderCell: renderRating,
      },
   ];

   return (
      <Box m="1.5rem 2.5rem">
         <Header title="REVIEWS" subtitle="List of Reviews" />
         <Box
            mt="40px"
            height="75vh"
            sx={{
               "& .MuiDataGrid-root": {
                  border: "none",
               },
               "& .MuiDataGrid-cell": {
                  borderBottom: "none",
               },
               "& .MuiDataGrid-columnHeaders": {
                  borderBottom: "none",
               },
               "& .MuiDataGrid-virtualScroller": {},
               "& .MuiDataGrid-footerContainer": {
                  borderTop: "none",
               },
               "& .MuiDataGrid-toolbarContainer .MuiButton-text": {},
            }}
         >
            <DataGrid
            slots={{
              loadingOverlay: LinearProgress
            }}
               loading={isLoading || !reviews}
               getRowId={(row) => row._id}
               getRowHeight={()=> 'auto'}
               rows={reviews || []}
               columns={columns}
            />
         </Box>
      </Box>
   );
};

export default Reviews;
