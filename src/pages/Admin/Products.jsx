import AddIcon from "@mui/icons-material/Add";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import LocalShippingIcon from "@mui/icons-material/LocalShipping";
import LocalShippingOutlinedIcon from '@mui/icons-material/LocalShippingOutlined';
import SearchIcon from "@mui/icons-material/Search";
import StarIcon from '@mui/icons-material/Star';
import StarBorderOutlined from "@mui/icons-material/StarBorderOutlined";
import { Box, Button, Card, CardActions, CardContent, CardMedia, Checkbox, CircularProgress, Collapse, FormControl, FormControlLabel, FormGroup, IconButton, InputBase, InputLabel, MenuItem, Modal, Rating, Select, Skeleton, Stack, TextField, Toolbar, Tooltip, Typography, alpha, styled, useMediaQuery } from '@mui/material';
import React, { useEffect, useRef, useState } from 'react';
import AlertField from "../../components/AlertField";
import { GlobalState } from '../../context/Context';
import { Product } from "../../context/actions";
import Header from './components/Header';


const SingleProduct = ({
   _id,
   name,
   category,
   company,
   inventory,
   price,
   featured,
   rating,
   freeShipping,
   description,
   image,
   slug,
   handleOpen,
   setIsEditing,
   setFormValues,
   setImage
}) => {
   const {productDispatch, productState} = GlobalState()
   const [isExpanded, setIsExpanded] = useState(false);

   const handleEdit = async () =>{
            setFormValues({
               name: name,
               company: company,
               category: category,
               description: description,
               price: price,
               id: _id,
               featured: featured,
               freeShipping: freeShipping
            });
            setImage(image.url);
            setIsEditing(true);
            handleOpen();
   }
   return (
      <Card
         sx={{
            backgroundImage: "none",
            borderRadius: "0.55rem",
            width: "100%",
         }}
      >
         <Stack direction="column" justifyContent="space-between">
            <CardMedia
               height="200"
               component="img"
               src={image.url}
               alt={name}
               sx={{
                  objectFit: "cover/contain",
                  aspectRatio: "4/3",
                  background: "no-repeat center fixed",
               }}
            />
            <CardContent>
               <Typography
                  sx={{ fontSize: 14 }}
                  gutterBottom
                  textTransform="capitalize"
               >
                  {category}
               </Typography>
               <Typography variant="h5" component="div">
                  {name}
               </Typography>
               <Typography sx={{ mb: "1.5rem" }}>
                  ₱ {Number(price).toFixed(2)}
               </Typography>
               <Rating value={rating} readOnly />
               <Typography textTransform="capitalize">{company}</Typography>
               <Typography variant="body2">{description}</Typography>
            </CardContent>
         </Stack>
         <CardActions>
            <Button
               variant="primary"
               size="small"
               onClick={() => setIsExpanded(!isExpanded)}
            >
               See More
            </Button>
         </CardActions>
         <Collapse in={isExpanded} timeout="auto" unmountOnExit>
            <CardContent
               sx={{ display: "flex", flexDirection: "row", alignItems:'start', justifyContent:'space-between', gap: ".5rem" }}
            >
               <Stack
                  direction="column"
                  spacing={2}
                  justifyContent="space-between"
               >
                  <Typography sx={{ fontWeight: 600 }}>
                     {inventory} items left
                  </Typography>
                  <Stack direction="row" spacing={1}>
                     <Tooltip title="Featured">
                        <Typography>
                           {featured ? <StarIcon /> : <StarBorderOutlined />}
                        </Typography>
                     </Tooltip>
                     <Tooltip title="Free Shipping">
                        <Typography>
                           {freeShipping ? (
                              <LocalShippingIcon />
                           ) : (
                              <LocalShippingOutlinedIcon />
                           )}
                        </Typography>
                     </Tooltip>
                  </Stack>
               </Stack>
               <Stack direction="row" spacing={1}>
                  <Tooltip title="Edit">
                     <IconButton onClick={() => handleEdit(_id)}>
                        <EditIcon />
                     </IconButton>
                  </Tooltip>
                  <Tooltip title="Delete">
                     <IconButton
                        onClick={async () => {
                           await Product.deleteProduct(productDispatch, _id);
                           await Product.getAllProducts(productDispatch);
                        }}
                     >
                        <DeleteIcon />
                     </IconButton>
                  </Tooltip>
               </Stack>
            </CardContent>
         </Collapse>
      </Card>
   );
};

const Search = styled("div")(({ theme }) => ({
   position: "relative",
   borderRadius: theme.shape.borderRadius,
   backgroundColor: alpha(theme.palette.common.white, 0.15),
   "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
   },
   marginLeft: 0,
   width: "100%",
   [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
   },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
   padding: theme.spacing(0, 2),
   height: "100%",
   position: "absolute",
   pointerEvents: "none",
   display: "flex",
   alignItems: "center",
   justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
   color: "inherit",
   "& .MuiInputBase-input": {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("sm")]: {
         width: "12ch",
         "&:focus": {
            width: "20ch",
         },
      },
   },
}));

const Products = () => {
  const {productState, productDispatch} = GlobalState()
  const {products, isLoading} = productState
  const isNonMobile = useMediaQuery("(min-width: 1000px)");
  console.log(products)
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [image, setImage]  = useState('')
  const [isEditing, setIsEditing] = useState(false)

      const style = {
         position: "absolute",
         top: "50%",
         left: "50%",
         transform: "translate(-50%, -50%)",
         width: 400,
         bgcolor: "background.paper",
         border: "none",
         borderRadius: '5px',
         boxShadow: 24,
         p: 4,
      };

    const nameRef = useRef();
    const companyRef = useRef();
    const categoryRef = useRef();
    const descriptionRef = useRef();
    const priceRef = useRef();

    const initialValues = {
       name:  "",
       company: "",
       category:  "",
       description:"",
       price: 0,
       featured: false,
       freeShipping: false,
    };

    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmit, setIsSubmit] = useState(false);

    const handleChecked = (e) =>{
      const {name, checked} = e.target
      setFormValues({...formValues, [name]: checked})
      
    }

    const handleChange = (e) => {
       const { name, value } = e.target;
       setFormValues({ ...formValues, [name]: value});
    };

    const handleImage = (e) => {
      const file = e.target.files[0]
      setFileToBase(file)
    }

    const setFileToBase = (file) =>{
      const reader = new FileReader();
      reader.readAsDataURL(file)
      reader.onloadend = () => {
         setImage(reader.result)
      }
    }
    const handleSubmit = async (e) => {
       e.preventDefault();
       setFormErrors(validate(formValues));
       setIsSubmit(true);

       const { name, category, description, company,price, featured, freeShipping} = formValues;

       const payload = { name, category, description, company, image, price, featured, freeShipping };
       if (Object.keys(formErrors).length === 0 && isSubmit) {
          await Product.createProduct(productDispatch, payload);
          await Product.getAllProducts(productDispatch) 
          setFormValues(initialValues)
          handleClose()
       }
    };

    const handleEdit = async (e, id)=>{
      e.preventDefault();
      setFormErrors(validate(formValues));
      setIsSubmit(true);

       const { name, category, description, company, price, freeShipping, featured } = formValues;

       const payload = {
          name,
          category,
          description,
          company,
          image,
          price,
          freeShipping,
          featured,
       };

        if (Object.keys(formErrors).length === 0 && isSubmit) {
           await Product.updateProduct(productDispatch, id, payload );
           await Product.getAllProducts(productDispatch);
           setFormValues(initialValues);
           handleClose();
        }
    }

    useEffect(() => {
       if (formErrors.name) {
          nameRef.current.focus();
       } else if (formErrors.company) {
          companyRef.current.focus();
       } else if (formErrors.category) {
          categoryRef.current.focus();
       } else if (formErrors.description) {
          descriptionRef.current.focus();
       }
       else if (formErrors.priceRef) {
          priceRef.current.focus();
       }
    }, [formErrors]);

    const validate = (values) => {
       const errors = {};
       // validator regex

       if (!values.name) {
          errors.name = "Product Name is required";
       }
       if (!values.company) {
          errors.company = "Category is required";
       } 
       if (!values.category) {
          errors.category = "Category is required";
       } 
       if (!values.description) {
          errors.description = "Product description is required";
       } 
       if (!values.price) {
          errors.price = "Product price is required";
       } 
       if(!image){
         errors.image = "Product Image is required"
       }
       return errors;
    };

  return (
     <>
        <Modal
           open={open}
           onClose={() => {
              handleClose();
              setIsEditing(false);
              setFormValues(initialValues);
              setImage("");
           }}
           aria-labelledby="modal-modal-title"
           aria-describedby="modal-modal-description"
        >
           <Box sx={style}>
              <Box
                 sx={{
                    width: "100%",
                    height: "100%",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                 }}
              >
                 <Box
                    sx={{
                       display: "flex",
                       flexDirection: "column",
                       gap: "1rem",
                       width: "400px",
                       border: "1px solid #ddd",
                       padding: "2.5rem 3.5rem",
                       borderRadius: "5px",
                    }}
                 >
                    <Typography variant="h4" textAlign="center">
                       {isEditing ? "Edit Product" : "Add Product"}
                    </Typography>
                    <FormControl
                       sx={{
                          fontSize: "18px",
                          width: "100%",
                          margin: ".5rem 0",
                       }}
                    >
                       <CardMedia
                          height="200"
                          component="img"
                          alt="image preview"
                          src={image}
                          sx={{
                             objectFit: "cover",
                             backgroundPosition: "center",
                             display: !image ? "none" : "block",
                             margin: ".5rem 0",
                          }}
                       />
                       <input
                          accept=".png, .jpg, .jpeg"
                          onChange={handleImage}
                          type="file"
                          name="image"
                       />
                    </FormControl>
                    <FormControl
                       sx={{
                          fontSize: "18px",
                          width: "100%",
                          margin: ".5rem 0",
                       }}
                    >
                       <TextField
                          id="outlined-name"
                          label="Product Name"
                          variant="outlined"
                          value={formValues.name}
                          name="name"
                          onChange={handleChange}
                          inputRef={nameRef}
                       />
                    </FormControl>
                    {formErrors.name && (
                       <AlertField
                          formError={formErrors.name}
                          title="Email Address"
                       />
                    )}
                    <FormControl
                       sx={{
                          fontSize: "18px",
                          width: "100%",
                          margin: ".5rem 0",
                       }}
                    >
                       <TextField
                          id="outlined-price"
                          label="Price"
                          variant="outlined"
                          name="price"
                          value={formValues.price}
                          type="number"
                          onChange={handleChange}
                          inputRef={priceRef}
                       />
                    </FormControl>
                    {formErrors.price && (
                       <AlertField
                          formError={formErrors.price}
                          title="Email Address"
                       />
                    )}

                    <FormControl fullWidth>
                       <InputLabel id="demo-simple-select-label">
                          Company
                       </InputLabel>
                       <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={formValues.company}
                          label="Company"
                          name="company"
                          onChange={handleChange}
                          inputRef={companyRef}
                       >
                          <MenuItem value=""></MenuItem>
                          <MenuItem value="ikea">Ikea</MenuItem>
                          <MenuItem value="liddy">Liddy</MenuItem>
                          <MenuItem value="marcos">Marcos</MenuItem>
                       </Select>
                    </FormControl>
                    {formErrors.company && (
                       <AlertField
                          formError={formErrors.company}
                          title="Username"
                       />
                    )}

                    <FormControl fullWidth>
                       <InputLabel id="demo-simple-select-label">
                          Category
                       </InputLabel>
                       <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={formValues.category}
                          name="category"
                          label="Company"
                          onChange={handleChange}
                          inputRef={categoryRef}
                       >
                          <MenuItem value=""></MenuItem>
                          <MenuItem value="office">Office</MenuItem>
                          <MenuItem value="kitchen">Kitchen</MenuItem>
                          <MenuItem value="bedroom">Bedroom</MenuItem>
                       </Select>
                    </FormControl>
                    {formErrors.category && (
                       <AlertField
                          formError={formErrors.category}
                          title="Category"
                       />
                    )}

                    <FormControl
                       sx={{
                          fontSize: "18px",
                          width: "100%",
                          margin: ".5rem 0",
                       }}
                    >
                       <TextField
                          id="outlined-description"
                          label="Description"
                          variant="outlined"
                          multiline
                          value={formValues.description}
                          minRows={4}
                          name="description"
                          onChange={handleChange}
                          inputRef={descriptionRef}
                       />
                    </FormControl>
                    {formErrors.description && (
                       <AlertField
                          formError={formErrors.description}
                          title="Username"
                       />
                    )}
                    <FormGroup>
                       <FormControlLabel
                          key={
                             "freeShipping_checkbox" + formValues.freeShipping
                          }
                          control={<Checkbox />}
                          name="freeShipping"
                          onChange={handleChecked}
                          checked={formValues.freeShipping}
                          label="Free Shipping"
                       />
                       <FormControlLabel
                          key={"freeShipping_checkbox"}
                          control={<Checkbox />}
                          onChange={handleChecked}
                          checked={formValues.featured}
                          name="featured"
                          label="Featured"
                       />
                    </FormGroup>
                    {productState.errorMessage &&
                    productState.errorMessage.includes("invalid") ? (
                       <AlertField
                          formError={productState.errorMessage}
                          title="Registration Failed"
                       />
                    ) : null}
                    {productState.errorMessage &&
                    productState.errorMessage.includes("exists") ? (
                       <AlertField
                          formError={productState.errorMessage}
                          title="Registration Failed"
                       />
                    ) : null}
                    {productState.serverResponse &&
                    productState.serverResponse.includes("verify") ? (
                       <AlertField
                          formError={productState.serverResponse}
                          title="Registration Success"
                          severity="success"
                       />
                    ) : null}
                    <Button
                       sx={{ marginTop: "1rem", fontSize: "18px" }}
                       variant="contained"
                       onClick={(e) => {
                          isEditing
                             ? handleEdit(e, formValues.id)
                             : handleSubmit(e);
                       }}
                       disabled={productState.isLoading}
                    >
                       {productState.isLoading ? (
                          <CircularProgress />
                       ) : isEditing ? (
                          "Edit Product"
                       ) : (
                          "Add Product"
                       )}
                    </Button>
                 </Box>
              </Box>
           </Box>
        </Modal>
        <Box m="1.5rem 2.5rem">
           <Header title="PRODUCTS" subtitle="List of products" />
           <Toolbar
              sx={{ backgroundColor: "grey", display: "flex", gap: "1rem" }}
           >
              <Typography
                 variant="h6"
                 noWrap
                 component="div"
                 sx={{ flexGrow: 1, display: { xs: "none", sm: "block" } }}
              >
                 MUI
              </Typography>
              <Search>
                 <SearchIconWrapper>
                    <SearchIcon />
                 </SearchIconWrapper>
                 <StyledInputBase
                    placeholder="Search…"
                    inputProps={{ "aria-label": "search" }}
                 />
              </Search>
              <Tooltip title="Add Product">
                 <IconButton onClick={handleOpen}>
                    <AddIcon />
                 </IconButton>
              </Tooltip>
           </Toolbar>
           {products || isLoading ? (
              <Box
                 mt="20px"
                 display="grid"
                 gridTemplateColumns="repeat(auto-fill, minmax(320px, 1fr))"
                 alignItems="start"
                 rowGap="20px"
                 columnGap="1.33%"
                 sx={{
                    "& > div": {
                       gridColumn: isNonMobile ? undefined : "span 4",
                    },
                 }}
              >
                 {products.map(
                    ({
                       _id,
                       name,
                       category,
                       company,
                       inventory,
                       image,
                       price,
                       featured,
                       averageRating,
                       freeShipping,
                       description,
                       slug,
                    }) => (
                       <SingleProduct
                          key={_id}
                          _id={_id}
                          name={name}
                          image={image}
                          category={category}
                          company={company}
                          rating={averageRating}
                          featured={featured}
                          freeShipping={freeShipping}
                          price={price}
                          inventory={inventory}
                          description={description}
                          slug={slug}
                          handleOpen={handleOpen}
                          setIsEditing={setIsEditing}
                          setOpen={setOpen}
                          setFormValues={setFormValues}
                          setImage={setImage}
                       />
                    )
                 )}
              </Box>
           ) : (
              <Skeleton animation="wave" variant="rectangular"></Skeleton>
           )}
        </Box>
     </>
  );
}

export default Products
