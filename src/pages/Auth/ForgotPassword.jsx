import React, { useEffect, useRef, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { GlobalState } from '../../context/Context';
import {Link as RouterLink} from 'react-router-dom'
import {
   Box,
   Button,
   CircularProgress,
   FormControl,
   Link,
   TextField,
   Typography,
} from "@mui/material";
import AlertField from '../../components/AlertField';
import { Auth } from '../../context/actions';

const ForgotPassword = () => {
  const {authState, authDispatch} = GlobalState()
  const navigate = useNavigate()
   const emailRef = useRef()
     const initialValues = {
        email: "",
     };
     const [formValues, setFormValues] = useState(initialValues);
     const [formErrors, setFormErrors] = useState({});
     const [isSubmit, setIsSubmit] = useState(false)
     const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value });
     };

     const handleSubmit = async (e) => {
      e.preventDefault();
      setFormErrors(validate(formValues));
      setIsSubmit(true);

      const { email } = formValues;
      const payload = { email };
      console.log(payload)

      if (Object.keys(formErrors).length === 0 && isSubmit) {
        Auth.forgotPassword(authDispatch, payload)
      }
   };

   const validate = (values) => {
      const errors = {};
      const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;

      if (!values.email) {
         errors.email = "Email Address is required";
      } else if (!emailRegex.test(values.email)) {
         errors.email = "Email Address is not a valid format";
      }
      return errors;
   };

   useEffect(() => {
      if (formErrors.email) {
         emailRef.current.focus();
      } 
   }, [formErrors]);


   return (
      <Box
         sx={{
            width: "100%",
            height: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
         }}
      >
         <Box
            sx={{
               display: "flex",
               flexDirection: "column",
               gap: "1rem",
               width: "350px",
               border: "1px solid #ddd",
               padding: "2.5rem 3.5rem",
               borderRadius: "5px",
            }}
         >
            <Typography variant="h4" textAlign="center">
               Forgot your Password?
            </Typography>
            <Typography
               variant="body1"
               textAlign="center"
               fontSize="18px"
               sx={{ color: "#555" }}
            >
               That's okay, it happens! Fill up the form and click on the button
               below to reset your password
            </Typography>
            <FormControl
               sx={{ fontSize: "18px", width: "100%", margin: "1rem 0" }}
            >
               <TextField
                  id="outlined-email"
                  label="Email Address"
                  variant="outlined"
                  name="email"
                  onChange={handleChange}
                  type="email"
                  inputRef={emailRef}
               />
            </FormControl>
            {formErrors.email && (
               <AlertField formError={formErrors.email} title="Email Address" />
            )}
            <Box
               sx={{
                  margin: "1rem 0",
                  display: "flex",
                  justifyContent: "end",
               }}
            >
               <Typography>
                  <Link component={RouterLink} to="/auth/login" style={{textDecoration:'none', fontWeight: '600'}}>
                     Back to Login
                  </Link>
               </Typography>
            </Box>
            <Button
               sx={{ marginTop: "1rem", fontSize: "18px" }}
               variant="contained"
               onClick={handleSubmit}
               disabled={authState.isLoading}
            >
               {authState.isLoading ? <CircularProgress /> : "Forgot Password"}
            </Button>
            {authState.serverResponse && (
               <AlertField
                  formError={authState.serverResponse}
                  title="Forgot password"
                  severity="info"
               />
            )}
         </Box>
      </Box>
   );
}

export default ForgotPassword
