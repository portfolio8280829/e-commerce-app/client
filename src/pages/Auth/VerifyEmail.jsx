import React, { useEffect } from 'react'
import { useLocation } from 'react-router-dom';
import { GlobalState } from '../../context/Context';
import {Box, Typography, Button} from '@mui/material'
import {Link as RouterLink} from 'react-router-dom'
import { Auth } from '../../context/actions';

function useQuery() {
   return new URLSearchParams(useLocation().search);
}


const VerifyEmail = () => {
  const {authDispatch} = GlobalState()
  const query = useQuery();

  const verifyToken = async () => {
    const verificationToken = query.get("token");
    const email = query.get('email')
    const payload = { verificationToken, email };
    await Auth.verifyEmail(authDispatch, payload);
  };

  useEffect(() => {
     verifyToken();
  }, []);

  return (
     <Box
        sx={{
           width: "100%",
           height: "100%",
           display: "flex",
           alignItems: "center",
           justifyContent: "center",
        }}
     >
        <Box
           sx={{
              display: "flex",
              flexDirection: "column",
              gap: "1rem",
              width: "350px",
              border: "1px solid #ddd",
              padding: "2.5rem 3.5rem",
              borderRadius: "5px",
           }}
        >
           <Typography textAlign="center" variant="h4">
              Account Verified
           </Typography>
           <Typography textAlign="center" sx={{ margin: "1rem 0" }}>
              Thank you for joining E-Commerce Website. Your account have been verified.
           </Typography>
           <Button
              variant="contained"
              textAlign="center"
              component={RouterLink}
              to="/auth/login"
           >
              Login
           </Button>
        </Box>
     </Box>
  );
}

export default VerifyEmail
