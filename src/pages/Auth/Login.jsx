import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import {
   Box,
   Button,
   CircularProgress,
   FormControl,
   IconButton,
   InputAdornment,
   InputLabel,
   OutlinedInput,
   Stack,
   TextField,
   Typography
} from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import { Link, Navigate, Link as RouterLink, useNavigate } from "react-router-dom";
import AlertField from "../../components/AlertField";
import { GlobalState } from "../../context/Context";
import { Auth } from "../../context/actions";

const Login = () => {
   const { authState, authDispatch, userState, userDispatch } = GlobalState();
   const emailRef = useRef();
   const passwordRef = useRef();
   const navigate = useNavigate();
   const initialValues = {
      email: "",
      password: "",
   };

   const [formValues, setFormValues] = useState(initialValues);
   const [formErrors, setFormErrors] = useState({});
   const [isSubmit, setIsSubmit] = useState(false);

   const [showPassword, setShowPassword] = useState(false);
   const handleClickShowPassword = () => {
      setShowPassword(!showPassword);
   };

   const handleChange = (e) => {
      const { name, value } = e.target;
      setFormValues({ ...formValues, [name]: value });
   };

   const handleResendVerification = async (e) => {
      e.preventDefault();
      const { email } = formValues;
      if (email) {
         Auth.resendVerification(authDispatch, email);
      }
   };

   const handleSubmit = async (e) => {
      e.preventDefault();
      setFormErrors(validate(formValues));
      setIsSubmit(true);

      const { email, password } = formValues;
      const payload = { email, password };
      if (Object.keys(formErrors).length === 0 && isSubmit) {
         await Auth.login(authDispatch, payload);
         await Auth.showCurrentUser(authDispatch);

         if(authState.user.role === 'admin'){
            navigate('/admin')
            window.location.reload();
         }
         if(authState.user.role === 'user'){
            navigate('/shop')
            window.location.reload();
         }
      }
   };
   const validate = (values) => {
      const errors = {};
      const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;

      if (!values.email) {
         errors.email = "Email Address is required";
      } else if (!emailRegex.test(values.email)) {
         errors.email = "Email Address is not a valid format";
      }
      if (!values.password) {
         errors.password = "Password is required";
      }
      return errors;
   };

   useEffect(() => {
      if (formErrors.email) {
         emailRef.current.focus();
      } else if (formErrors.password) {
         passwordRef.current.focus();
      }
   }, [formErrors]);

   return (
      <Box
         sx={{
            width: "100%",
            height: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
         }}
      >
         <Stack
            sx={{
               display: "flex",
               flexDirection: "column",
               gap: "1rem",
               width: "350px",
               border: "1px solid #ddd",
               padding: "2.5rem 3.5rem",
               borderRadius: "5px",
            }}
         >
            <Typography variant="h4" textAlign="center">
               Sign In
            </Typography>
            <FormControl
               sx={{ fontSize: "18px", width: "100%", margin: "1rem 0" }}
            >
               <TextField
                  id="outlined-email"
                  label="Email Address"
                  variant="outlined"
                  name="email"
                  onChange={handleChange}
                  required
                  inputRef={emailRef}
               />
            </FormControl>
            {formErrors.email && (
               <AlertField formError={formErrors.email} title="Email Address" />
            )}
            <FormControl
               sx={{ width: "100%", fontSize: "18px", margin: "1rem 0" }}
               variant="outlined"
            >
               <InputLabel htmlFor="outlined-adornment-password">
                  Password
               </InputLabel>
               <OutlinedInput
                  id="outlined-adornment-password"
                  type={showPassword ? "text" : "password"}
                  name="password"
                  onChange={handleChange}
                  endAdornment={
                     <InputAdornment position="end">
                        <IconButton
                           aria-label="toggle password visibility"
                           onClick={handleClickShowPassword}
                           edge="end"
                        >
                           {showPassword ? (
                              <VisibilityOffIcon />
                           ) : (
                              <VisibilityIcon />
                           )}
                        </IconButton>
                     </InputAdornment>
                  }
                  label="Password"
                  inputRef={passwordRef}
               />
            </FormControl>
            {formErrors.password && (
               <AlertField formError={formErrors.password} title="Password" />
            )}
            <Box
               sx={{
                  margin: "1rem 0",
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
               }}
            >
               <Link
                  underline="none"
                  component={RouterLink}
                  style={{
                     textDecoration: "none",
                     color: "inherit",
                     fontWeight: 600,
                  }}
                  to="/auth/register"
               >
                  Sign Up
               </Link>
               <Link
                  underline="none"
                  component={RouterLink}
                  style={{
                     textDecoration: "none",
                     color: "inherit",
                     fontWeight: 600,
                  }}
                  to="/auth/forgot-password"
               >
                  Forgot Password?
               </Link>
            </Box>
            <Button
               sx={{ marginTop: "1rem", fontSize: "18px" }}
               variant="contained"
               onClick={handleSubmit}
               disabled={authState.isLoading}
            >
               {authState.isLoading ? <CircularProgress /> : "Sign In"}
            </Button>

            {authState.errorMessage &&
            authState.errorMessage.includes("verify") ? (
               <>
                  <AlertField
                     formError={authState.errorMessage}
                     title="Email Verification"
                     severity="info"
                  />
                  <Button
                     variant="contained"
                     color="info"
                     onClick={handleResendVerification}
                  >
                     Resend Link
                  </Button>
               </>
            ) : null}

            {authState.errorMessage &&
            authState.errorMessage.includes("Credentials") ? (
               <AlertField
                  formError={authState.errorMessage}
                  severity="error"
                  title="Sign In Failed"
               />
            ) : null}

            {authState.errorMessage &&
            authState.errorMessage.includes("recheck") ? (
               <AlertField
                  formError={authState.errorMessage}
                  severity="info"
                  title="Email Verification Sent"
               />
            ) : null}
         </Stack>
      </Box>
   );
};

export default Login;
