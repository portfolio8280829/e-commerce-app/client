import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import {
   Button,
   Box,
   Typography,
   FormControl,
   TextField,
   FormHelperText,
   InputLabel,
   OutlinedInput,
   InputAdornment,
   IconButton,
   Link,
   CircularProgress
} from "@mui/material";
import {Link as RouterLink} from 'react-router-dom'
import React, { useEffect, useRef, useState } from "react";
import AlertField from "../../components/AlertField";
import { GlobalState } from "../../context/Context";
import { Auth } from "../../context/actions";

const Register = () => {
  const { authState, authDispatch} = GlobalState()
   const emailRef = useRef();
   const userRef = useRef();
   const passwordRef = useRef();
   const confirmPasswordRef = useRef();
   const initialValues = {
      email: "",
      password: "",
      username: "",
      confirmPassword: "",
   };

   const [showPassword, setShowPassword] = useState(false);
   const handleClickShowPassword = () => {
      setShowPassword(!showPassword);
   };
console.log(authState)
   const [showConfirmPassword, setShowConfirmPassword] = useState(false);
   const handleClickConfirmShowPassword = () => {
      setShowConfirmPassword(!showConfirmPassword);
   };

   const [formValues, setFormValues] = useState(initialValues);
   const [formErrors, setFormErrors] = useState({});
   const [isSubmit, setIsSubmit] = useState(false);

   const handleChange = (e) => {
      const { name, value } = e.target;
      setFormValues({ ...formValues, [name]: value });
   };
   const handleSubmit = async (e) => {
      e.preventDefault();
      setFormErrors(validate(formValues));
      setIsSubmit(true);

      const { email, password, username } = formValues;
      const payload = { email, password, username };
      if (Object.keys(formErrors).length === 0 && isSubmit) {
        Auth.register(authDispatch, payload)
      }
   };

   useEffect(() => {
      if (formErrors.email) {
         emailRef.current.focus();
      } else if (formErrors.username) {
         userRef.current.focus();
      } else if (formErrors.password) {
         passwordRef.current.focus();
      } else if (formErrors.confirmPassword) {
         confirmPasswordRef.current.focus();
      }
   }, [formErrors]);

   const validate = (values) => {
      const errors = {};
      // validator regex
      const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
      const userRegex = /^[A-Za-z0-9]{4,16}$/;
      const passwordRegex =
         /^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,20}$/;

      if (!values.username) {
         errors.username = "Username is required";
      } else if (!userRegex.test(values.username)) {
         errors.username =
            "Username should be 4-16 characters and shouldn't include any special characters";
      }
      if (!values.email) {
         errors.email = "Email Address is required";
      } else if (!emailRegex.test(values.email)) {
         errors.email = "Email Address is not a valid format";
      }
      if (!values.password) {
         errors.password = "Password is required";
      } else if (!passwordRegex.test(values.password)) {
         errors.password = `Use at least 6 characters one uppercase letter one lowercase letter and one number and a special character.`;
      }
      if (!values.confirmPassword) {
         errors.confirmPassword = "Confirm Password is required";
      } else if (!passwordRegex.test(values.confirmPassword)) {
         errors.confirmPassword = `Use at least 6 characters one uppercase letter one lowercase letter and one number and a special character in your password`;
      } else if (values.confirmPassword !== values.password) {
         errors.confirmPassword = "Passwords do not match";
      }
      return errors;
   };
   return (
      <Box
         sx={{
            width: "100%",
            height: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
         }}
      >
         <Box
            sx={{
               display: "flex",
               flexDirection: "column",
               gap: "1rem",
               width: "350px",
               border: "1px solid #ddd",
               padding: "2.5rem 3.5rem",
               borderRadius: "5px",
            }}
         >
            <Typography variant="h4" textAlign="center">
               Sign Up
            </Typography>
            <FormControl
               sx={{ fontSize: "18px", width: "100%", margin: ".5rem 0" }}
            >
               <TextField
                  id="outlined-email"
                  label="Email Address"
                  variant="outlined"
                  name="email"
                  onChange={handleChange}
                  inputRef={emailRef}
               />
               <FormHelperText>
                  We'll never share your email address
               </FormHelperText>
            </FormControl>
            {formErrors.email && (
               <AlertField formError={formErrors.email} title="Email Address" />
            )}

            <FormControl
               sx={{ fontSize: "18px", width: "100%", margin: ".5rem 0" }}
            >
               <TextField
                  id="outlined-username"
                  label="Username"
                  variant="outlined"
                  name="username"
                  onChange={handleChange}
                  inputRef={userRef}
               />
            </FormControl>
            {formErrors.username && (
               <AlertField formError={formErrors.username} title="Username" />
            )}
            <FormControl
               sx={{ width: "100%", fontSize: "18px", margin: ".5rem 0" }}
               variant="outlined"
            >
               <InputLabel htmlFor="outlined-adornment-password">
                  Password
               </InputLabel>
               <OutlinedInput
                  id="outlined-adornment-password"
                  type={showPassword ? "text" : "password"}
                  endAdornment={
                     <InputAdornment position="end">
                        <IconButton
                           aria-label="toggle password visibility"
                           onClick={handleClickShowPassword}
                           edge="end"
                        >
                           {showPassword ? (
                              <VisibilityOffIcon />
                           ) : (
                              <VisibilityIcon />
                           )}
                        </IconButton>
                     </InputAdornment>
                  }
                  label="Password"
                  name="password"
                  onChange={handleChange}
                  inputRef={passwordRef}
               />
            </FormControl>
            {formErrors.password && (
               <AlertField formError={formErrors.password} title="Password" />
            )}
            <FormControl
               sx={{ width: "100%", fontSize: "18px", margin: ".5rem 0" }}
               variant="outlined"
            >
               <InputLabel htmlFor="outlined-adornment-confirm-password">
                  Confirm Password
               </InputLabel>
               <OutlinedInput
                  id="outlined-adornment-confirm-password"
                  type={showConfirmPassword ? "text" : "password"}
                  endAdornment={
                     <InputAdornment position="end">
                        <IconButton
                           aria-label="toggle password visibility"
                           onClick={handleClickConfirmShowPassword}
                           edge="end"
                        >
                           {showConfirmPassword ? (
                              <VisibilityOffIcon />
                           ) : (
                              <VisibilityIcon />
                           )}
                        </IconButton>
                     </InputAdornment>
                  }
                  label="Confirm Password"
                  name="confirmPassword"
                  onChange={handleChange}
                  inputRef={confirmPasswordRef}
               />
            </FormControl>
            {formErrors.confirmPassword && (
               <AlertField
                  formError={formErrors.confirmPassword}
                  title="Confirm Password"
               />
            )}
            <Box
               sx={{
                  margin: "1rem 0",
                  display: "flex",
               }}
            >
               <Typography>
                  Already have an account?{" "}
                  <Link
                     style={{ textDecoration: "none", fontWeight: 600 }}
                     component={RouterLink}
                     to="/auth/login"
                  >
                     Sign In
                  </Link>
               </Typography>
            </Box>
         
            {authState.errorMessage &&
            authState.errorMessage.includes("invalid") ? (
               <AlertField
                  formError={authState.errorMessage}
                  title="Registration Failed"
               />
            ) : null}
            {authState.errorMessage &&
            authState.errorMessage.includes("exists") ? (
               <AlertField
                  formError={authState.errorMessage}
                  title="Registration Failed"
               />
            ) : null}
            {authState.serverResponse &&
            authState.serverResponse.includes("verify") ? (
               <AlertField
                  formError={authState.serverResponse}
                  title="Registration Success"
                  severity="success"
               />
            ) : null}
            <Button
               sx={{ marginTop: "1rem", fontSize: "18px" }}
               variant="contained"
               onClick={handleSubmit}
               disabled={authState.isLoading}
            >
               {authState.isLoading ? <CircularProgress /> : "Sign Up"}
            </Button>
         </Box>
      </Box>
   );
};

export default Register;
