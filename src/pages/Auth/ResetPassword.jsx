import React, { useEffect, useRef, useState } from 'react'
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import {
   Box,
   Button,
   CircularProgress,
   FormControl,
   IconButton,
   InputAdornment,
   InputLabel,
   Link,
   OutlinedInput,
   Typography,
} from "@mui/material";
import { useLocation } from 'react-router-dom';
import { GlobalState } from '../../context/Context';
import AlertField from '../../components/AlertField';
import {Link as RouterLink} from 'react-router-dom'
import { Auth } from '../../context/actions';
function useQuery() {
   return new URLSearchParams(useLocation().search);
}

const ResetPassword = () => {
  const {authState, authDispatch} = GlobalState()
   const query = useQuery();
   const [showPassword, setShowPassword] = useState(false);
   const handleClickShowPassword = () => {
      setShowPassword(!showPassword);
   };
   const [showConfirmPassword, setShowConfirmPassword] = useState(false);
   const handleClickShowConfirmPassword = () => {
      setShowConfirmPassword(!showConfirmPassword);
   };

   const passwordRef = useRef();
   const confirmPasswordRef = useRef();

   const initialValues = {
      email: "",
   };
   const [formValues, setFormValues] = useState(initialValues);
   const [formErrors, setFormErrors] = useState({});
   const [isSubmit, setIsSubmit] = useState(false);
   

   const handleChange = (e) => {
      const { name, value } = e.target;
      setFormValues({ ...formValues, [name]: value });
   };

   const handleSubmit = async (e) => {
      e.preventDefault();
      setFormErrors(validate(formValues));
      setIsSubmit(true);
      const { password } = formValues;
      const resetPassword = {
         password,
         token: query.get("token"),
         email: query.get("email"),
      };

      if (Object.keys(formErrors).length === 0 && isSubmit) {
          Auth.resetPassword(authDispatch, resetPassword)
      }
   };

   const validate = (values) => {
      const errors = {};
      const passwordRegex =
         /^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,20}$/;

      if (!values.password) {
         errors.password = "Password is required";
      } else if (!passwordRegex.test(values.password)) {
         errors.password = "New Password is not a valid format";
      }
      if (!values.confirmPassword) {
         errors.confirmPassword = "Confirm Password is required";
      } else if (!passwordRegex.test(values.confirmPassword)) {
         errors.confirmPassword = "Confirm Password is not a valid format";
      } else if (values.confirmPassword !== values.password) {
         errors.confirmPassword = "Passwords do not match";
      }
      return errors;
   };

   useEffect(() => {
      if (formErrors.password) {
         passwordRef.current.focus();
      } else if (formErrors.confirmPassword) {
         confirmPasswordRef.current.focus();
      }
   }, [formErrors]);
  return (
     <Box
        sx={{
           width: "100%",
           height: "100%",
           display: "flex",
           alignItems: "center",
           justifyContent: "center",
        }}
     >
        <Box
           sx={{
              display: "flex",
              flexDirection: "column",
              gap: "1rem",
              width: "350px",
              border: "1px solid #ddd",
              padding: "2.5rem 3.5rem",
              borderRadius: "5px",
           }}
        >
           <Typography variant="h4" textAlign="center">
              Reset Password
           </Typography>
           <FormControl
              sx={{ width: "100%", fontSize: "18px", margin: "1rem 0" }}
              variant="outlined"
           >
              <InputLabel htmlFor="outlined-adornment-password">
                 New Password
              </InputLabel>
              <OutlinedInput
                 id="outlined-adornment-password"
                 type={showPassword ? "text" : "password"}
                 name="password"
                 onChange={handleChange}
                 endAdornment={
                    <InputAdornment position="end">
                       <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          edge="end"
                       >
                          {showPassword ? (
                             <VisibilityOffIcon />
                          ) : (
                             <VisibilityIcon />
                          )}
                       </IconButton>
                    </InputAdornment>
                 }
                 label="New Password"
                 inputRef={passwordRef}
              />
           </FormControl>
           {formErrors.password && (
              <AlertField
                 formError={formErrors.password}
                 title="New Password"
              />
           )}
           <FormControl
              sx={{ width: "100%", fontSize: "18px", margin: "1rem 0" }}
              variant="outlined"
           >
              <InputLabel htmlFor="outlined-adornment-confirmPassword">
                 Confirm Password
              </InputLabel>
              <OutlinedInput
                 id="outlined-adornment-confirmPassword"
                 type={showConfirmPassword ? "text" : "password"}
                 name="confirmPassword"
                 onChange={handleChange}
                 endAdornment={
                    <InputAdornment position="end">
                       <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowConfirmPassword}
                          edge="end"
                       >
                          {showConfirmPassword ? (
                             <VisibilityOffIcon />
                          ) : (
                             <VisibilityIcon />
                          )}
                       </IconButton>
                    </InputAdornment>
                 }
                 label="Confirm Password"
                 inputRef={confirmPasswordRef}
              />
           </FormControl>
           {formErrors.confirmPassword && (
              <AlertField
                 formError={formErrors.confirmPassword}
                 title="Confirm Password"
              />
           )}
           <Box
              sx={{
                 margin: "1rem 0",
                 display: "flex",
                 justifyContent: "end",
              }}
           >
              <Typography>
                 <Link
                    component={RouterLink}
                    to="/auth/login"
                    sx={{ textDecoration: "none", fontWeight: 600 }}
                 >
                    Back to Login
                 </Link>
              </Typography>
           </Box>
           <Button
              sx={{ marginTop: "1rem", fontSize: "18px" }}
              variant="contained"
              onClick={handleSubmit}
              disabled={authState.isLoading}
           >
              {authState.isLoading ? <CircularProgress /> : "Reset Password"}
           </Button>
           {authState.errorMessage &&
           authState.errorMessage.includes("reset") ? (
              <AlertField
                 formError={authState.errorMessage}
                 title="Reset password"
                 severity="info"
              />
           ) : null}
        </Box>
     </Box>
  );
}

export default ResetPassword
