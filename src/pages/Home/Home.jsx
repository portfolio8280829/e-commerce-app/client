import { CircularProgress, Container } from '@mui/material'
import React from 'react'
import { GlobalState } from '../../context/Context'

const Home = () => {
  const {authState} = GlobalState()

  if(authState.isLoading) {
   return (
      <Container>
         <CircularProgress />;
      </Container>
   );
  } 
 
  return <Container>
  </Container>
}

export default Home
