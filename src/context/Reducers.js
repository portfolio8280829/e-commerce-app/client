import { auth, cart, order, product, product_filter, review, theme, user } from './constants';

export const globalReducer = (state,action) => {
  switch(action.type) {
    case theme.TOGGLE_LIGHT:
      return {
        ...state,
        theme: 'light'
    }

    case theme.TOGGLE_DARK:
      return {
        ...state,
        theme: 'dark'
    }
    default:
    return state;
  }
}


export const authReducer = (state, action) => {
  switch(action.type){
    // Login
    case auth.LOGIN_REQUEST:
      return {...state,
      isLoading: true,
      errorMessage: null
    }
    
    case auth.LOGIN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        user: action.payload.user,
        isAuthenticated: true,
    }

    case auth.LOGIN_ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.error
    }

    // Register
    case auth.REGISTER_REQUEST:
      return {
        ...state,
        isLoading: true,
        errorMessage: null
    }

    case auth.REGISTER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        serverResponse: action.response.msg
    }

    case auth.REGISTER_ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.error
    }

    // Logout
    case auth.LOGOUT_REQUEST: 
      return {
        ...state,
        isLoading: true,
        errorMessage: null
    }

    case auth.LOGOUT_SUCCESS:
      return{
        ...state,
          user: {
          email:"", 
          userId: "", 
          role: "", 
          username: ""
        },
        isLoading: false,
        isAuthenticated: false,
    }

    case auth.LOGOUT_ERROR:
      return {
        ...state,
        errorMessage: action.error,
        isLoading: false,
    }

    // Verify Email
    case auth.VERIFY_EMAIL_REQUEST:
      return {
        ...state,
        isLoading: true,
        errorMessage: null
    }

    case auth.VERIFY_EMAIL_SUCCESS:
      return {
        ...state,
        isLoading: false,
    }

    case auth.VERIFY_EMAIL_ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.error
    }
    // Resend Verification Link
    case auth.RESEND_VERIFICATION_REQUEST:
      return {
        ...state,
        isLoading: true,
        errorMessage: null
    }

    case auth.RESEND_VERIFICATION_SUCCESS:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.payload.msg
    }
    
    case auth.RESEND_VERIFICATION_ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.payload.msg
    }
    // Reset Password
    case auth.RESET_PASSWORD_REQUEST:
      return {
        ...state,
        isLoading: true,
        errorMessage: null
    }

    case auth.RESET_PASSWORD_SUCCESS:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.response
    }
    
    case auth.RESET_PASSWORD_ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.payload.msg
    }

    // Forgot Password
    case auth.FORGOT_PASSWORD_REQUEST:
      return {
        ...state,
        isLoading: true,
        errorMessage: null
    }

    case auth.FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        isLoading: false,
        serverResponse: action.response.msg
    }
    
    case auth.FORGOT_PASSWORD_ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.error
    }

    case auth.SHOW_CURRENT_USER_REQUEST:
      return {
        ...state,
        isLoading: true,
        errorMessage: null
    }

    case auth.SHOW_CURRENT_USER_SUCCESS:
      return {
        ...state,
        user: action.payload.user,
        isAuthenticated: true,
        isLoading: false,
    }
    
    case auth.SHOW_CURRENT_USER_ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.error
    }

    default:
      return state;
  }
}

export const userReducer = (state,action) => {
   switch(action.type){
    // show current user
   
    // get all users
    case user.GET_ALL_USERS_REQUEST:
      return {
        ...state,
        isLoading: true,
          errorMessage: null
    }

    case user.GET_ALL_USERS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        users: action.payload
    }

    case user.GET_ALL_USERS_ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.error
    }
    // update user
    case user.UPDATE_USER_REQUEST:
      return {
        ...state,
        isLoading: true,
          errorMessage: null
    }

    case user.UPDATE_USER_SUCCESS:
      return {
        ...state,
        users: action.payload.users
    }

    case user.UPDATE_USER_ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.error
    }
    // update user password
    case user.UPDATE_USER_PASSWORD_REQUEST:
      return {
        ...state,
        isLoading: true,
          errorMessage: null
    }

    case user.UPDATE_USER_PASSWORD_SUCCESS:
      return {
        ...state,
        isLoading: false
    }

    case user.UPDATE_USER_PASSWORD_ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.error
    }
    // get single user
    case user.GET_SINGLE_USER_REQUEST:
      return {
        ...state,
        isLoading: true,
          errorMessage: null
    }

    case user.GET_SINGLE_USER_SUCCESS:
      return {
        ...state,
        isLoading: false
    }

    case user.GET_SINGLE_USER_ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.error
    }

    default:
      return state;
  }
}

export const productReducer = (state, action) =>{
  switch(action.type){
      case product.CREATE_PRODUCT_REQUEST:
      return {
        ...state,
        isLoading: true,
    }

    case product.CREATE_PRODUCT_SUCCESS:
      return {
        ...state,
        isLoading: false
    }

    case product.CREATE_PRODUCT_ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.error
    }

    case product.GET_ALL_PRODUCTS_REQUEST:
      return {
        ...state,
        isLoading: true,
    }

    case product.GET_ALL_PRODUCTS_SUCCESS:
      return {
        ...state,
        products: action.payload.products,
        isLoading: false
    }

    case product.GET_ALL_PRODUCTS_ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.error
    }
    case product.GET_SINGLE_PRODUCT_REQUEST:
      return {
        ...state,
        isLoading: true,
    }

    case product.GET_SINGLE_PRODUCT_SUCCESS:
      return {
        ...state,
        product: action.payload.product,
        isLoading: false
    }

    case product.GET_SINGLE_PRODUCT_ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.error
    }

    case product.UPDATE_SINGLE_PRODUCT_REQUEST:
      return {
        ...state,
        isLoading: true,
    }

    case product.UPDATE_SINGLE_PRODUCT_SUCCESS:
      return {
        ...state,
        isLoading: false
    }

    case product.UPDATE_SINGLE_PRODUCT_ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.error
    }
    
    case product.DELETE_SINGLE_PRODUCT_REQUEST:
      return {
        ...state,
        isLoading: true,
    }

    case product.DELETE_SINGLE_PRODUCT_SUCCESS:
      return {
        ...state,
        isLoading: false
    }

    case product.DELETE_SINGLE_PRODUCT_ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.error
    }
    default:
      return state
  }
}

export const productFilterReducer = (state, action) =>{
  switch(action.type){

    case product_filter.SORT_BY_PRICE:
    return {...state, Price: action.payload}

    case product_filter.SORT_BY_COLOR:
    return {...state, Color: action.payload}

    case product_filter.SORT_BY_CATEGORY:
    return {...state, Category: action.payload}

    case product_filter.SORT_BY_COMPANY:
    return {...state, Company: action.payload}

    case product_filter.SORT_BY_RATING:
    return {...state, Rating: action.payload}

    case product_filter.SORT_BY_FEATURED:
    return {...state, Featured: !state.Featured}

    case product_filter.SORT_BY_SHIPPING:
    return {...state, FreeShipping: !state.FreeShipping}

    case product_filter.SORT_BY_SALES:
    return {...state, Sales: !state.Sales}

    case product_filter.SORT_BY_DATE:
    return {...state, Date: !state.Date}

    case product_filter.FILTER_BY_SEARCH:
    return {...state, Search: action.payload}

    case product_filter.CLEAR_FILTERS:
    return {
      Rating: 0,
      Price: 0,
      Category: "",
      Company: "",
      Search: "",
      Featured: false,
      TopSales: false,
      FreeShipping: false,
      Color: false,
      Date: false,
    }

    default: 
    return state
  }
}

export const reviewReducer = (state, action) =>{
 switch(action.type){
 case review.GET_ALL_REVIEWS_REQUEST:
      return {
        ...state,
        isLoading: true,
    }

    case review.GET_ALL_REVIEWS_SUCCESS:
      return {
        ...state,
        reviews: action.payload.reviews,
        isLoading: false
    }

    case review.GET_ALL_REVIEWS_ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.error
    }
    default:
      return state
  }
}

export const orderReducer = (state, action) =>{
     switch(action.type){
     case order.REQUEST:
      return {
        ...state,
        isLoading: true,
    }

      case order.ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.error
    }

    case order.GET_ALL_ORDERS_SUCCESS:
      return {
        ...state,
        orders: action.payload.orders,
        isLoading: false
    }

    case order.SHOW_USERS_ORDERS_SUCCESS:
      return {
        ...state,
        orders: action.payload.orders,
        isLoading: false
    }


    case order.CREATE_ORDER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        cart: [],
      }

    default:
      return state
  }
}

export const cartReducer = (state, action) => {
  switch(action.type){
    
    case cart.REQUEST: 
    return {
      ...state,
      isLoading: true
    }

    case cart.ERROR:
      return {
        ...state, 
        isLoading: false,
        errorMessage: action.error
    }

    case cart.GET_USER_CART_SUCCESS:
      return {
        ...state,
        cart: action.payload.cartItems,
        isLoading: false,
      }
    case cart.ADD_TO_CART_SUCCESS:
      return {
        ...state,
        isLoading: false,
      }
    case cart.DELETE_CART_ITEM_SUCCESS:
      return {
        ...state,
        isLoading: false
      }

    default: 
    return state

  }
}