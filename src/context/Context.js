import { createContext, useContext, useEffect, useMemo, useReducer } from "react";
import { authReducer, cartReducer, orderReducer, productFilterReducer, productReducer, reviewReducer, userReducer } from "./Reducers";
import { Auth, Cart, Order, Product, Review, User } from "./actions";

const GlobalContext = createContext()

const Context = ({children}) => {
  const [authState, authDispatch] = useReducer(authReducer, {
    user: {
      username: "",
      email:"", 
      userId: "", 
      role: "", 
    },
    isAuthenticated: false,
    isLoading: false,
    errorMessage: null,
    serverResponse: null
  })

  const [userState, userDispatch] = useReducer(userReducer, {
    users: [],
    user: {},
    isLoading: false,
    errorMessage: null,
    serverResponse: null
  })

  const [productState, productDispatch] = useReducer(productReducer, {
    products: [],
    productImages: [],
    product: {},
    isLoading: false,
    errorMessage: null,
    serverResponse: null
  })

  const [productFilters, productFiltersDispatch] = useReducer(productFilterReducer, {
    Rating: 0,
    Price: 0,
    Category: "",
    Company: "",
    Search: "",
    Featured: false,
    TopSales: false,
    FreeShipping: false,
    Color: false,
    Date: false,
  })

  const [reviewState, reviewDispatch] = useReducer(reviewReducer, {
    reviews: [],
    review: {},
    isLoading: false,
    errorMessage: null,
    serverResponse: null
  })

   const [orderState, orderDispatch] = useReducer(orderReducer, {
    orders: [],
    order: {},
    isLoading: false,
    errorMessage: null,
    serverResponse: null
  })

  const [cartState, cartDispatch] = useReducer(cartReducer, {
    cart: [],
    isLoading: false,
    errorMessage: null,
    serverResponse: null
  })
 

  useEffect(()=>{
    if(authState.user.role === "user"){
      Cart.getUserCart(cartDispatch)
    }
  },[authState])

  
  useEffect(()=>{
    Auth.showCurrentUser(authDispatch)
  },[])

  useEffect(()=>{
    if(authState.user.role === "admin"){
      User.getAllUsers(userDispatch)
    }
  },[authState])

  useEffect(() => {
    Product.getAllProducts(productDispatch);
  }, []);

  
  useEffect(()=>{
    if(authState.user.role === "admin"){
      Order.getAllOrders(orderDispatch)
    }
  },[authState])
  
  useEffect(()=>{
    if(authState.user.role === "user"){
      Order.getUserOrders(orderDispatch)
    }
  },[authState])

  useEffect(()=>{
    if(authState.user.role === "admin"){
        Review.getAllReviews(reviewDispatch)
    }
  },[authState])

  const value = useMemo(
    ()=> ({
    authState, authDispatch,
    userState, userDispatch,
    productState, productDispatch,
    productFilters, productFiltersDispatch,
    reviewState, reviewDispatch,
    orderState, orderDispatch,
    cartState, cartDispatch
  }),[
    authState, 
    userState, 
    productState, 
    productFilters,
    reviewState, 
    orderState,
    cartState
  ])

  return (
    <GlobalContext.Provider value={value}>
      {children}
    </GlobalContext.Provider>
  )
}

export default Context

export const GlobalState = () => {
  return useContext(GlobalContext)
}