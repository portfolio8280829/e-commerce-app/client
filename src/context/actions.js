import axios from 'axios'
import { AUTH_URL, CART_URL, ORDER_URL, PRODUCT_URL, REVIEW_URL, USER_URL } from '../assets/constants/url'
import { auth, cart, order, product, review, user } from './constants'


const Auth = {
  login : async (dispatch, login) => {
    try{
      dispatch({type: auth.LOGIN_REQUEST})
      return await axios
        .post(`${AUTH_URL}/login`, login , { withCredentials: true })
        .then((response)=>{
          return response.data
        })
        .then((data)=>{
          dispatch({type: auth.LOGIN_SUCCESS, payload: data})
        })
        .catch((error)=>{
          dispatch({type: auth.LOGIN_ERROR, error: error.response.data.msg})
        })
    }
    catch(error){
      dispatch({type: auth.LOGIN_ERROR, error: "Something went wrong. Please try again later."})
    }
  },

  register : async (dispatch, register) => {
    try{
      dispatch({type: auth.REGISTER_REQUEST})
      return await axios
        .post(`${AUTH_URL}/register`, register)
        .then((response)=>{
          return response.data
        })
        .then((data)=>{
          dispatch({type: auth.REGISTER_SUCCESS, response: data})
        })
        .catch((error)=>{
           dispatch({type: auth.REGISTER_ERROR, error: error.response.data.msg})
        })
    }
    catch(error){
      dispatch({type: auth.REGISTER_ERROR, error:"Something went wrong. Please try again later." })
    }
  },

  logout : async (dispatch) => {
    try{
      dispatch({type: auth.LOGOUT_REQUEST})
      return await axios
        .delete(`${AUTH_URL}/logout`, {withCredentials: true})
        .then((response)=>{
          return response.data
        })
        .then((data)=>{
          dispatch({type: auth.LOGOUT_SUCCESS})
        })
        .catch((error)=>{
          dispatch({type: auth.LOGOUT_ERROR, error: error.response.data.msg})
        })
    }
    catch(error){
      dispatch({type: auth.LOGOUT_ERROR, error:"Something went wrong. Please try again later."})
    }
  },

  verifyEmail : async(dispatch, verifyEmail) => {
    try{
      dispatch({type: auth.VERIFY_EMAIL_REQUEST})
      return await axios
        .post(`${AUTH_URL}/verify-email`, verifyEmail ,{withCredentials: true})
        .then((response)=>{
          return response.data
        })
        .then((data)=>{
          dispatch({type: auth.VERIFY_EMAIL_SUCCESS})
        })
        .catch((error)=>{
          dispatch({type: auth.VERIFY_EMAIL_ERROR, error: error.response.data.msg})
        })
    }
    catch(error){
      dispatch({type: auth.VERIFY_EMAIL_ERROR, error:"Something went wrong. Please try again later."})
    }
  },

  resendVerification : async(dispatch, email) => {
    try{
      dispatch({type: auth.RESEND_VERIFICATION_REQUEST})
      return await axios
        .post(`${AUTH_URL}/resend-verification`, {email} , { withCredentials: true })
        .then((response)=>{
          return response.data
        })
        .then((data)=>{
          dispatch({type: auth.RESEND_VERIFICATION_SUCCESS, payload: data})
        })
        .catch((error)=>{
          dispatch({type: auth.RESEND_VERIFICATION_ERROR, error: error.response.data.msg})
        })
    }
    catch(error){
      dispatch({type: auth.RESEND_VERIFICATION_ERROR, error:"Something went wrong. Please try again later."})
    }
  },

  resetPassword : async(dispatch, oldPassword)=>{
    try{
      dispatch({type: auth.RESET_PASSWORD_REQUEST})
      return await axios
        .post(`${AUTH_URL}/reset-password`, oldPassword ,{withCredentials: true})
        .then((response)=>{
          return response.data
        })
        .then((data)=>{
          dispatch({type: auth.RESET_PASSWORD_SUCCESS, response: data})
        })
        .catch((error)=>{
          dispatch({type: auth.RESET_PASSWORD_ERROR, error: error.response.data.msg})
        })
    }
    catch(error){
      dispatch({type: auth.RESET_PASSWORD_ERROR, error:"Something went wrong. Please try again later."})
    }
  },

  forgotPassword : async(dispatch, email)=>{
    try{
      dispatch({type: auth.FORGOT_PASSWORD_REQUEST})
      return await axios
        .post(`${AUTH_URL}/forgot-password`, email , {withCredentials: true})
        .then((response)=>{
          return response.data
        })
        .then((data)=>{
          dispatch({type: auth.FORGOT_PASSWORD_SUCCESS, response: data})
        })
        .catch((error)=>{
          dispatch({type: auth.FORGOT_PASSWORD_ERROR, error: error.response.data.msg})
        })
    }
    catch(error){
      dispatch({type: auth.FORGOT_PASSWORD_ERROR, error:"Something went wrong. Please try again later."})
    }
  },

  showCurrentUser : async (dispatch) => {
    try{
      dispatch({type: auth.SHOW_CURRENT_USER_REQUEST})
      return await axios
        .get(`${USER_URL}/showMe`,{withCredentials: true})
        .then((response)=>{
          return response.data
        })
        .then((data)=>{
          dispatch({type: auth.SHOW_CURRENT_USER_SUCCESS, payload: data})
        })
        .catch((error)=>{
          dispatch({type: auth.SHOW_CURRENT_USER_ERROR, error: error.response.data.msg})
        })
    }
    catch(error){
      dispatch({type: auth.SHOW_CURRENT_USER_ERROR, error:"Something went wrong. Please try again later."})
    }
  },
}

const User = {
  getAllUsers: async(dispatch) => {
    try{
      dispatch({type: user.GET_ALL_USERS_REQUEST})
      return await axios
        .get(`${USER_URL}/`,{withCredentials: true})
        .then((response)=>{
          return response.data
        })
        .then((data)=>{
          dispatch({type: user.GET_ALL_USERS_SUCCESS, payload: data.users})
        })
        .catch((error)=>{
          dispatch({type: user.GET_ALL_USERS_ERROR, error: error.response.data.msg})
        })
    }
    catch(error){
      dispatch({type: user.GET_ALL_USERS_ERROR, error:"Something went wrong. Please try again later."})
    }
  },

  updateUser : async(dispatch, updatedUser) =>{
    try{
      dispatch({type: user.UPDATE_USER_PASSWORD_REQUEST})
      return await axios
        .patch(`${USER_URL}/updateUser`, updatedUser,{withCredentials: true})
        .then((response)=> {
          return response.data
        })
        .then((data)=>{
          dispatch({type: user.UPDATE_USER_PASSWORD_SUCCESS, user: data})
        })
        .catch((error)=>{
          dispatch({type: user.UPDATE_USER_PASSWORD_ERROR, error: error.response.data.msg})
        })
    }
    catch(error){
      dispatch({type: user.UPDATE_USER_PASSWORD_ERROR, error:"Something went wrong. Please try again later."})
    }
  },

  updateUserPassword : async(dispatch, updatedPassword) =>{
    try{
      dispatch({type: user.UPDATE_USER_PASSWORD_REQUEST})
      return await axios
        .patch(`${USER_URL}/updateUserPassword`, updatedPassword, {withCredentials: true})
        .then((response)=> {
          return response.data
        })
        .then((data)=>{
          dispatch({type:  user.UPDATE_USER_PASSWORD_SUCCESS, user: data})
        })
        .catch((error)=>{
          dispatch({type:  user.UPDATE_USER_PASSWORD_ERROR, error: error.response.data.msg})
        })
    }
    catch(error){
      dispatch({type:  user.UPDATE_USER_PASSWORD_ERROR, error:"Something went wrong. Please try again later."})
    }
  },

  getSingleUser : async(dispatch, id) =>{
    try{
      dispatch({type: user.GET_SINGLE_USER_REQUEST})
      return await axios
        .patch(`${USER_URL}/${id}`, {withCredentials: true})
        .then((response)=> {
          return response.data
        })
        .then((data)=>{
          dispatch({type: user.GET_SINGLE_USER_SUCCESS, user: data})
        })
        .catch((error)=>{
          dispatch({type: user.GET_SINGLE_USER_ERROR, error: error.response.data.msg})
        })
    }
    catch(error){
      dispatch({type: user.GET_SINGLE_USER_ERROR, error:"Something went wrong. Please try again later."})
    }
  }
}

const Product = {
  createProduct : async(dispatch, productBody) => {
     try{
      dispatch({type:product.CREATE_PRODUCT_REQUEST})
              await axios.post(`${PRODUCT_URL}/`, productBody, {withCredentials: true, headers:{ "Content-Type" : "multipart/form-data"}})
              .then((response)=>{
                return response.data
              })
              .then((data)=>{
                dispatch({type: product.CREATE_PRODUCT_SUCCESS})
              })
              .catch((error)=>{
                dispatch({type: product.CREATE_PRODUCT_ERROR, error: error.response.data.msg})
              })
    }
    catch(error){
      dispatch({type: product.CREATE_PRODUCT_ERROR, error:"Something went wrong. Please try again later."})
    }
  },

  getAllProducts : async(dispatch) => {
    try{
        dispatch({type:product.GET_ALL_PRODUCTS_REQUEST})
          await axios
          .get(`${PRODUCT_URL}/`,{withCredentials:true})
          .then((response)=> {
            return response.data
          })
          .then((data)=>{
              
              dispatch({type: product.GET_ALL_PRODUCTS_SUCCESS, payload: data})
          })
          .catch((error)=>{
            dispatch({type: product.GET_ALL_PRODUCTS_ERROR, error: error.response.data.msg})
          })
    }
    catch(error){
      dispatch({type: product.GET_ALL_PRODUCTS_ERROR, error:"Something went wrong. Please try again later."})
    }
  },
  getSpecificProduct : async(dispatch,slug) => {
    try{
        dispatch({type:product.GET_SINGLE_PRODUCT_REQUEST})
          await axios
          .get(`${PRODUCT_URL}/${slug}`,{withCredentials:true})
          .then((response)=> {
            return response.data
          })
          .then((data)=>{
              dispatch({type: product.GET_SINGLE_PRODUCT_SUCCESS, payload: data})
          })
          .catch((error)=>{
            dispatch({type: product.GET_SINGLE_PRODUCT_ERROR, error: error.response.data.msg})
          })
    }
    catch(error){
      dispatch({type: product.GET_SINGLE_PRODUCT_ERROR, error:"Something went wrong. Please try again later."})
    }
  },

  updateProduct : async(dispatch, id, updateBody) =>{
   try{
        dispatch({type:product.UPDATE_SINGLE_PRODUCT_REQUEST})
          await axios
          .patch(`${PRODUCT_URL}/${id}`, updateBody ,{withCredentials: true, headers:{ "Content-Type" : "multipart/form-data"}})
          .then((response)=> {
            return response.data
          })
          .then((data)=>{
              dispatch({type: product.UPDATE_SINGLE_PRODUCT_SUCCESS})
          })
          .catch((error)=>{
            dispatch({type: product.UPDATE_SINGLE_PRODUCT_ERROR, error: error.response.data.msg})
          })
    }
    catch(error){
      dispatch({type: product.UPDATE_SINGLE_PRODUCT_ERROR, error:"Something went wrong. Please try again later."})
    }
  },

  deleteProduct : async(dispatch, id) => {
    try{
        dispatch({type:product.DELETE_SINGLE_PRODUCT_REQUEST})
          await axios
          .delete(`${PRODUCT_URL}/${id}`,{withCredentials: true, headers:{ "Content-Type" : "multipart/form-data"}})
          .then((response)=> {
            return response.data
          })
          .then((data)=>{
              dispatch({type: product.DELETE_SINGLE_PRODUCT_SUCCESS})
          })
          .catch((error)=>{
            dispatch({type: product.DELETE_SINGLE_PRODUCT_ERROR, error: error.response.data.msg})
          })
    }
    catch(error){
      dispatch({type: product.DELETE_SINGLE_PRODUCT_ERROR, error:"Something went wrong. Please try again later."})
    }
  }
}

const Review = {
  getAllReviews : async (dispatch) => {
    try{
      dispatch({type: review.GET_ALL_REVIEWS_REQUEST})
      return await axios
        .get(`${REVIEW_URL}/`, {withCredentials: true })
        .then((response)=>{
          return response.data
        })
        .then((data)=>{
          dispatch({type: review.GET_ALL_REVIEWS_SUCCESS, payload: data})
        })
        .catch((error)=>{
          dispatch({type: review.GET_ALL_REVIEWS_ERROR, error: error.response.data.msg})
        })
    }
    catch(error){
      dispatch({type: review.GET_ALL_REVIEWS_ERROR, error: "Something went wrong. Please try again later."})
    }
  },
}

const Order = {
  getAllOrders : async (dispatch) => {
    try{
      dispatch({type: order.REQUEST})
      return await axios
        .get(`${ORDER_URL}/`, {withCredentials: true })
        .then((response)=>{
          return response.data
        })
        .then((data)=>{
          dispatch({type: order.GET_ALL_ORDERS_SUCCESS, payload: data})
        })
        .catch((error)=>{
          dispatch({type: order.ERROR, error: error.response.data.msg})
        })
    }
    catch(error){
      dispatch({type: order.ERROR, error: "Something went wrong. Please try again later."})
    }
  },

  getUserOrders : async (dispatch) => {
    try{
      dispatch({type: order.REQUEST})
      return await axios
        .get(`${ORDER_URL}/showAllMyOrders`, {withCredentials: true })
        .then((response)=>{
          return response.data
        })
        .then((data)=>{
          dispatch({type: order.SHOW_USERS_ORDERS_SUCCESS, payload: data})
        })
        .catch((error)=>{
          dispatch({type: order.ERROR, error: error.response.data.msg})
        })
    }
    catch(error){
      dispatch({type: order.ERROR, error: "Something went wrong. Please try again later."})
    }
  },

  createOrder : async (dispatch, cart) => {
    try{
      dispatch({type: order.REQUEST})
      return await axios
        .post(`${ORDER_URL}/`, cart , { withCredentials: true })
        .then((response)=>{
          return response.data
        })
        .then((data)=>{
          console.log(data)
          dispatch({type: order.CREATE_ORDER_SUCCESS, payload: data})
        })
        .catch((error)=>{
          dispatch({type: order.ERROR, error: error.response.data.msg})
        })
    }
    catch(error){
      dispatch({type: order.ERROR, error: "Something went wrong. Please try again later."})
    }
  },
}

const Cart = {
  getUserCart : async(dispatch) =>{
    try{
      dispatch({type: cart.REQUEST})
      return await axios
      .get(`${CART_URL}/`, {withCredentials:true})
      .then((response)=>{
        return response.data
      })
      .then((data)=>{
        dispatch({type: cart.GET_USER_CART_SUCCESS, payload: data})
      })
      .catch((error)=>{
        dispatch({type: cart.ERROR, error: error.response.data.msg})
      })
    }
    catch(error){
      dispatch({type: cart.ERROR, error:"Something went wrong. Please try again later."})
    }
  },
  
  addToCart : async(dispatch, cartItem)=>{
      try{
      dispatch({type: cart.REQUEST})
      return await axios
      .post(`${CART_URL}/`, cartItem ,{withCredentials:true})
      .then((response)=>{
        return response.data
      })
      .then((data)=>{
        dispatch({type: cart.ADD_TO_CART_SUCCESS})
      })
      .catch((error)=>{
        dispatch({type: cart.ERROR, error: error.response.data.msg})
      })
    }
    catch(error){
      dispatch({type: cart.ERROR, error:"Something went wrong. Please try again later."})
    }
  },

  deleteFromCart : async(dispatch, id)=>{
    console.log(id)
     try{
      dispatch({type: cart.REQUEST})
      return await axios
      .delete(`${CART_URL}/${id}`,{withCredentials:true})
      .then((response)=>{
        return response.data
      })
      .then((data)=>{
        console.log(data)
        dispatch({type: cart.DELETE_CART_ITEM_SUCCESS})
      })
      .catch((error)=>{
        dispatch({type: cart.ERROR, error: error.response.data.msg})
      })
    }
    catch(error){
      dispatch({type: cart.ERROR, error:"Something went wrong. Please try again later."})
    }
  }
}


export {
  Auth,
  User,
  Product,
  Review,
  Order,
  Cart
}