const theme = {
  TOGGLE_LIGHT: 'TOGGLE_LIGHT',
  TOGGLE_DARK: 'TOGGLE_DARK'
}

const auth ={
  CLEAR_RESPONSE :'CLEAR_RESPONSE',
  LOGIN_REQUEST: "LOGIN_REQUEST",
  LOGIN_SUCCESS: "LOGIN_SUCCESS",
  LOGIN_ERROR: "LOGIN_ERROR",

  REGISTER_REQUEST: "REGISTER_REQUEST",
  REGISTER_SUCCESS: "REGISTER_SUCCESS",
  REGISTER_ERROR: "REGISTER_ERROR",

  LOGOUT_REQUEST: "LOGOUT_REQUEST",
  LOGOUT_SUCCESS: "LOGOUT_SUCCESS",
  LOGOUT_ERROR: "LOGOUT_ERROR",

  VERIFY_EMAIL_REQUEST: "VERIFY_EMAIL_REQUEST",
  VERIFY_EMAIL_SUCCESS: "VERIFY_EMAIL_SUCCESS",
  VERIFY_EMAIL_ERROR: "VERIFY_EMAIL_ERROR",

  RESEND_VERIFICATION_REQUEST: "RESEND_VERIFICATION_REQUEST",
  RESEND_VERIFICATION_SUCCESS: "RESEND_VERIFICATION_SUCCESS",
  RESEND_VERIFICATION_ERROR: "RESEND_VERIFICATION_ERROR",

  RESET_PASSWORD_REQUEST: "RESET_PASSWORD_REQUEST",
  RESET_PASSWORD_SUCCESS: "RESET_PASSWORD_SUCCESS",
  RESET_PASSWORD_ERROR: "RESET_PASSWORD_ERROR",

  FORGOT_PASSWORD_REQUEST: "FORGOT_PASSWORD_REQUEST",
  FORGOT_PASSWORD_SUCCESS: "FORGOT_PASSWORD_SUCCESS",
  FORGOT_PASSWORD_ERROR: "FORGOT_PASSWORD_ERROR",

  SHOW_CURRENT_USER_REQUEST: "SHOW_CURRENT_USER_REQUEST",
  SHOW_CURRENT_USER_SUCCESS: "SHOW_CURRENT_USER_SUCCESS",
  SHOW_CURRENT_USER_ERROR: "SHOW_CURRENT_USER_ERROR",
}

const user = {
  GET_ALL_USERS_REQUEST: "GET_ALL_USERS_REQUEST",
  GET_ALL_USERS_SUCCESS: "GET_ALL_USERS_SUCCESS",
  GET_ALL_USERS_ERROR: "GET_ALL_USERS_ERROR",

  GET_SINGLE_USER_REQUEST: "GET_SINGLE_USER_REQUEST",
  GET_SINGLE_USER_SUCCESS: "GET_SINGLE_USER_SUCCESS",
  GET_SINGLE_USER_ERROR: "GET_SINGLE_USER_ERROR",

  UPDATE_USER_REQUEST: "UPDATE_USER_REQUEST",
  UPDATE_USER_SUCCESS: "UPDATE_USER_SUCCESS",
  UPDATE_USER_ERROR: "UPDATE_USER_ERROR",

  UPDATE_USER_PASSWORD_REQUEST: "UPDATE_USER_PASSWORD_REQUEST",
  UPDATE_USER_PASSWORD_SUCCESS: "UPDATE_USER_PASSWORD_SUCCESS",
  UPDATE_USER_PASSWORD_ERROR: "UPDATE_USER_PASSWORD_ERROR",
}

const product = {
  CREATE_PRODUCT_REQUEST : 'CREATE_PRODUCT_REQUEST',
  CREATE_PRODUCT_SUCCESS : 'CREATE_PRODUCT_SUCCESS',
  CREATE_PRODUCT_ERROR : 'CREATE_PRODUCT_ERROR',

  GET_ALL_PRODUCTS_REQUEST : 'GET_ALL_PRODUCTS_REQUEST',
  GET_ALL_PRODUCTS_SUCCESS : 'GET_ALL_PRODUCTS_SUCCESS',
  GET_ALL_PRODUCTS_ERROR : 'GET_ALL_PRODUCTS_ERROR',

  GET_SINGLE_PRODUCT_REQUEST : 'GET_SINGLE_PRODUCT_REQUEST',
  GET_SINGLE_PRODUCT_SUCCESS : 'GET_SINGLE_PRODUCT_SUCCESS',
  GET_SINGLE_PRODUCT_ERROR : 'GET_SINGLE_PRODUCT_ERROR',

  UPDATE_SINGLE_PRODUCT_REQUEST : 'UPDATE_SINGLE_PRODUCT_REQUEST',
  UPDATE_SINGLE_PRODUCT_SUCCESS : 'UPDATE_SINGLE_PRODUCT_SUCCESS',
  UPDATE_SINGLE_PRODUCT_ERROR : 'UPDATE_SINGLE_PRODUCT_ERROR',

  DELETE_SINGLE_PRODUCT_REQUEST : 'DELETE_SINGLE_PRODUCT_REQUEST',
  DELETE_SINGLE_PRODUCT_SUCCESS : 'DELETE_SINGLE_PRODUCT_SUCCESS',
  DELETE_SINGLE_PRODUCT_ERROR : 'DELETE_SINGLE_PRODUCT_ERROR',

  GET_SINGLE_PRODUCT_REVIEW_REQUEST : 'GET_SINGLE_PRODUCT_REVIEW_REQUEST',
  GET_SINGLE_PRODUCT_REVIEW_SUCCESS : 'GET_SINGLE_PRODUCT_REVIEW_SUCCESS',
  GET_SINGLE_PRODUCT_REVIEW_ERROR : 'GET_SINGLE_PRODUCT_REVIEW_ERROR',
}
const product_filter = {
  SORT_BY_RATING : "SORT_BY_RATING",
  SORT_BY_FEATURED: "SORT_BY_FEATURED",
  SORT_BY_SALES: "SORT_BY_SALES",
  SORT_BY_SHIPPING: "SORT_BY_SHIPPING",
  SORT_BY_PRICE: "SORT_BY_PRICE",
  SORT_BY_CATEGORY: "SORT_BY_CATEGORY",
  SORT_BY_COMPANY: "SORT_BY_COMPANY",
  SORT_BY_COLOR: "SORT_BY_COLOR",
  SORT_BY_DATE: "SORT_BY_DATE",
  FILTER_BY_SEARCH: "FILTER_BY_SEARCH",
  CLEAR_FILTERS: "CLEAR_FILTERS",
}

const review = {
  GET_ALL_REVIEWS_REQUEST : 'GET_ALL_REVIEWS_REQUEST',
  GET_ALL_REVIEWS_SUCCESS : 'GET_ALL_REVIEWS_SUCCESS',
  GET_ALL_REVIEWS_ERROR : 'GET_ALL_REVIEWS_ERROR',

  UPDATE_SINGLE_REVIEW_REQUEST : 'UPDATE_SINGLE_REVIEW_REQUEST',
  UPDATE_SINGLE_REVIEW_SUCCESS : 'UPDATE_SINGLE_REVIEW_SUCCESS',
  UPDATE_SINGLE_REVIEW_ERROR : 'UPDATE_SINGLE_REVIEW_ERROR',

  GET_SINGLE_REVIEW_REQUEST : 'GET_SINGLE_REVIEW_REQUEST',
  GET_SINGLE_REVIEW_SUCCESS : 'GET_SINGLE_REVIEW_SUCCESS',
  GET_SINGLE_REVIEW_ERROR : 'GET_SINGLE_REVIEW_ERROR',
  
  CREATE_REVIEW_REQUEST : 'CREATE_REVIEW_REQUEST',
  CREATE_REVIEW_SUCCESS : 'CREATE_REVIEW_SUCCESS',
  CREATE_REVIEW_ERROR : 'CREATE_REVIEW_ERROR',

  DELETE_REVIEW_REQUEST : 'DELETE_REVIEW_REQUEST',
  DELETE_REVIEW_SUCCESS : 'DELETE_REVIEW_SUCCESS',
  DELETE_REVIEW_ERROR : 'DELETE_REVIEW_ERROR',

  SHOW_ALL_USERS_ORDERS_REQUEST : 'SHOW_ALL_USERS_ORDERS_REQUEST',
  SHOW_ALL_USERS_ORDERS_SUCCESS : 'SHOW_ALL_USERS_ORDERS_SUCCESS',
  SHOW_ALL_USERS_ORDERS_ERROR : 'SHOW_ALL_USERS_ORDERS_ERROR',
}

const order = {
  GET_ALL_ORDERS_SUCCESS : 'GET_ALL_ORDERS_SUCCESS',

  UPDATE_SINGLE_ORDER_SUCCESS : 'UPDATE_SINGLE_ORDER_SUCCESS',

  GET_SINGLE_ORDER_SUCCESS : 'GET_SINGLE_ORDER_SUCCESS',
  
  CREATE_ORDER_SUCCESS : 'CREATE_ORDER_SUCCESS',

  SHOW_USERS_ORDERS_SUCCESS : 'SHOW_USERS_ORDERS_SUCCESS',

  REQUEST: "REQUEST",
  ERROR: "ERROR"
}

const cart = {
  GET_USER_CART_SUCCESS : "GET_USER_CART_SUCCESS",
  ADD_TO_CART_SUCCESS: "ADD_TO_CART_SUCCESS",
  DELETE_CART_ITEM_SUCCESS: "DELETE_CART_ITEM_SUCCESS",
  REQUEST: "REQUEST",
  ERROR: "ERROR"
}

export {
  theme,
  auth,
  user,
  product,
  review,
  order,
  product_filter,
  cart
}