import { createTheme } from "@mui/material";
import { useMemo, useState } from "react";
import theme from "./theme";

export const useColorTheme = () =>{ 
  const [mode, setMode] = useState(localStorage.getItem('theme') ?? 'light')

  const toggleColorMode = () =>{
    setMode((prev)=> {
      localStorage.setItem('theme', prev === 'light' ? 'dark' : 'light')
      return prev === 'light' ? 'dark' : 'light'
    })
  }

  const modifiedTheme = useMemo(
    ()=> 
      createTheme({
        ...theme,
        palette: {
          ...theme.palette,
          mode,
        }
      }), 
    [mode]
  )

  return {
     theme: modifiedTheme,
     mode,
     toggleColorMode,
  };
}