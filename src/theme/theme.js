import { grey } from "@mui/material/colors"
import colors from "../assets/styles/colors"

const theme = {
  palette: {
    primary: grey,
  }
}

export default theme