import { Alert, AlertTitle, Stack, Typography } from '@mui/material';
import React from 'react';

const AlertField = ({formError, title, severity="error"}) => {
  return (
     <Stack sx={{ width: "100%" }} spacing={2}>
        <Alert severity={severity}>
           <AlertTitle>{title}</AlertTitle>
           <Typography
              variant="body1"
              display="block"
              sx={{ whiteSpace: "pre-wrap" }}
           >
              {formError} 
           </Typography>
        </Alert>
     </Stack>
  );
}

export default AlertField
