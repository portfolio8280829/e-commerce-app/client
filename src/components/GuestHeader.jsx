import Brightness7Icon from "@mui/icons-material/Brightness7";
import ModeNightIcon from "@mui/icons-material/ModeNight";
import {
  AppBar,
  Box,
  Button,
  CircularProgress,
  IconButton,
  Stack,
  Toolbar,
  Typography,
} from "@mui/material";
import React from 'react';
import { Link } from "react-router-dom";
import { FlexBetween } from "../assets/styles/styles";
import { useThemeContext } from "../theme/ThemeContextProvider";
import { GlobalState } from "../context/Context";

const GuestHeader = () => {
   const { mode, toggleColorMode } = useThemeContext();
   const {authState} = GlobalState()
      if (authState.isLoading) {
         return (
            <Box
               sx={{
                  width: "100%",
                  height: "100%",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
               }}
            >
               <CircularProgress />
            </Box>
         );
      }
   return (
      <>
         <Box
            sx={{
               display: "flex",
               justifyContent: "end",
               padding: "0.5rem",
            }}
         >
            <IconButton onClick={toggleColorMode} color="inherit" size="small">
               {mode === "dark" ? <Brightness7Icon /> : <ModeNightIcon />}
            </IconButton>
         </Box>
         <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
               <Toolbar>
                  <Stack direction="row" sx={FlexBetween}>
                     <Typography
                        variant="h6"
                        component="div"
                        sx={{ flexGrow: 1 }}
                     >
                        E-Commerce Website
                     </Typography>
                     <Stack direction="row">
                        <Button
                           component={Link}
                           to="/"
                           color="inherit"
                           sx={{ mr: ".5rem" }}
                        >
                           Home
                        </Button>
                        <Button
                           component={Link}
                           to="/auth/login"
                           color="inherit"
                        >
                           Sign In | Sign Up
                        </Button>
                     </Stack>
                  </Stack>
               </Toolbar>
            </AppBar>
         </Box>
      </>
   )
}

export default GuestHeader
