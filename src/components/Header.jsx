import { CircularProgress } from '@mui/material';
import React from 'react';
import { GlobalState } from '../context/Context';
import { useThemeContext } from '../theme/ThemeContextProvider';
import GuestHeader from './GuestHeader';
const Header = () => {
  const {mode, toggleColorMode} = useThemeContext()
  const { authState, authDispatch } = GlobalState();

  let content
  content = <GuestHeader />

  authState.isLoading &&  <CircularProgress />
  return (
     <>
     {content}
     </>
  );
}


export default Header
