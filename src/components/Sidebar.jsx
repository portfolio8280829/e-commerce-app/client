import React, { useEffect, useState } from 'react';

import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import GroupIcon from "@mui/icons-material/Group";
import HomeIcon from '@mui/icons-material/Home';
import LogoutIcon from "@mui/icons-material/Logout";
import PieChartIcon from "@mui/icons-material/PieChart";
import PointOfSaleIcon from "@mui/icons-material/PointOfSale";
import RateReviewIcon from "@mui/icons-material/RateReview";
import ReceiptIcon from "@mui/icons-material/Receipt";
import SettingsIcon from "@mui/icons-material/Settings";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import { Box, Drawer, IconButton, List, ListItem, ListItemButton, ListItemIcon, ListItemText, Typography } from '@mui/material';
import { useLocation, useNavigate } from 'react-router-dom';
import { FlexBetween } from '../assets/styles/styles';
import { GlobalState } from '../context/Context';
import { Auth } from '../context/actions';


const navItems = [
   {
      text: "Dashboard",
      icon: <HomeIcon />,
   },
   {
      text: "Products",
      icon: <ShoppingCartIcon />,
   },
   {
      text: "Customers",
      icon: <GroupIcon />,
   },
   {
      text: "Orders",
      icon: <ReceiptIcon />,
   },
   {
      text: "Reviews",
      icon: <RateReviewIcon />,
   },
   {
      text: "Overview",
      icon: <PointOfSaleIcon />,
   },
   {
      text: "Breakdown",
      icon: <PieChartIcon />,
   },
   {
      text: "Settings",
      icon: <SettingsIcon />,
   },
];

const Sidebar = ({
  user, 
  drawerWidth, 
  isSidebarOpen,
  setIsSidebarOpen, 
  isNonMobile
}) => {
  const {pathname} = useLocation()
  const [active, setIsActive] = useState("")
  const navigate = useNavigate()
  const {authDispatch} = GlobalState()

  useEffect(()=>{
    setIsActive(pathname.substring(1))
  }, [pathname])
  
  return (
     <Box>
        {isSidebarOpen && (
           <Drawer
              open={isSidebarOpen}
              onClose={() => setIsSidebarOpen(false)}
              variant="persistent"
              anchor="left"
              sx={{
                 width: drawerWidth,
                 "& .MuiDrawer-paper": {
                    boxSixing: "border-box",
                    borderWidth: isNonMobile ? 0 : "2px",
                    width: drawerWidth,
                 },
              }}
           >
              <Box width="100%">
                 <Box m="1.5rem 2rem 2rem 3rem">
                    <Box sx={FlexBetween}>
                       <Box display="flex" alignItems="center" gap="0.5rem">
                          <Typography variant="h6" fontWeight="bold">
                             E-Commerce Website
                          </Typography>
                       </Box>
                       {!isNonMobile && (
                          <IconButton
                             onClick={() => setIsSidebarOpen(!isSidebarOpen)}
                          >
                             <ChevronLeftIcon />
                          </IconButton>
                       )}
                    </Box>
                 </Box>
                 <List>
                    {navItems.map(({ text, icon }) => {
                       if (!icon) {
                          return (
                             <Typography
                                key={text}
                                sx={{ m: "2.25rem 0 1rem 3rem" }}
                             >
                                {text}
                             </Typography>
                          );
                       }
                       const lcText = text.toLowerCase();

                       return (
                          <ListItem key={text} disablePadding>
                             <ListItemButton
                                onClick={() => {
                                   navigate(`/admin/${lcText}`);
                                   setIsActive(lcText);
                                }}
                             >
                                <ListItemIcon>{icon}</ListItemIcon>
                                <ListItemText primary={text} />
                                {active === lcText && (
                                   <ChevronRightIcon sx={{ ml: "auto" }} />
                                )}
                             </ListItemButton>
                          </ListItem>
                       );
                    })}
                    <ListItem disablePadding>
                       <ListItemButton
                          onClick={() => {
                                Auth.logout(authDispatch);
                                navigate("/home");
                          }}
                       >
                          <ListItemIcon>
                             <LogoutIcon />
                          </ListItemIcon>
                          <ListItemText primary="Logout" />
                       </ListItemButton>
                    </ListItem>
                 </List>
              </Box>

              <Box position="absolute" bottom="2rem">
                 <Box
                    sx={FlexBetween}
                    style={{ alignItems: "start" }}
                    gap="1rem"
                    m="1.5rem 2rem 0 3rem"
                 >
                    <Box textAlign="left">
                       <Typography fontWeight="bold" fontSize="0.9rem">
                          {user.role.toUpperCase()}
                       </Typography>
                       <Typography fontSize="0.8rem">
                          {user.username}
                       </Typography>
                    </Box>
                 </Box>
              </Box>
           </Drawer>
        )}
     </Box>
  );
}

export default Sidebar
