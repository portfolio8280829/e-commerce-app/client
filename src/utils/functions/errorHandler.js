export const errorHandler = (error) =>{
  const {request, response} = error;
  if(response){
    const {message} = response.data;
    const status = response.status;
    return {message, status}
  }
  else if(request){
    return {
      message: "Server Timed out",
      status: 503,
    }
  }
  else{
    return {message: "Something went wrong, Please try again later"}
  }
}