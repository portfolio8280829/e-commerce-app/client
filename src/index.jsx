import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import App from './App';
import Context from './context/Context';
import { ThemeContextProvider } from './theme/ThemeContextProvider';
import './index.css'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
   <React.StrictMode>
      <ThemeContextProvider>
         <Context>
            <Router>
               <Routes>
                  <Route path="/*" element={<App />} />
               </Routes>
            </Router>
         </Context>
      </ThemeContextProvider>
   </React.StrictMode>
);

