
import { CssBaseline, ThemeProvider } from '@mui/material';
import { Navigate, Route, Routes } from 'react-router-dom';
import { GlobalState } from './context/Context';
import AdminLayout from './layouts/AdminLayout';
import Layout from './layouts/Layout';
import UserLayout from './layouts/UserLayout';
import {
   Breakdown,
   Customers,
   Dashboard,
   ForgotPassword,
   Home,
   Login,
   Notfound,
   Orders,
   Overview,
   Products,
   Register,
   ResetPassword,
   Reviews,
   Settings,
   UserCart,
   UserGuard,
   UserJobOrder,
   UserOrderItem,
   UserOrders,
   UserProducts,
   UserReviews,
   UserSettings,
   VerifyEmail
} from './pages';
import AdminGuard from './pages/Guards/AdminGuard';
import { useThemeContext } from './theme/ThemeContextProvider';

const App = () => {
const { theme } = useThemeContext();
const {authState,userState} = GlobalState()

return (
   <ThemeProvider theme={theme}>
      <CssBaseline />
      <Routes>
         <Route path="/" element={<Layout />}>
            <Route path="/" element={<Navigate replace to="/home" />} />
            <Route path="/home" element={<Home />} />

            <Route path="auth" exact>
               <Route path="login">
                  <Route index element={<Login />} />
               </Route>

               <Route path="register">
                  <Route index element={<Register />} />
               </Route>

               <Route path="forgot-password">
                  <Route index element={<ForgotPassword />} />
               </Route>

               <Route path="reset-password">
                  <Route index element={<ResetPassword />} />
               </Route>

               <Route path="verify-email">
                  <Route index element={<VerifyEmail />} />
               </Route>
            </Route>
            <Route path="*" element={<Notfound />} />
         </Route>

         <Route
            path="/admin"
            element={
               <AdminGuard>
                  <AdminLayout />
               </AdminGuard>
            }
         >
            <Route index element={<Navigate to="dashboard" replace />} />
            <Route path="dashboard" element={<Dashboard />} />
            <Route path="products" element={<Products />} />
            <Route path="customers" element={<Customers />} />
            <Route path="orders" element={<Orders />} />
            <Route path="reviews" element={<Reviews />} />
            <Route path="overview" element={<Overview />} />
            <Route path="breakdown" element={<Breakdown />} />
            <Route path="settings" element={<Settings />} />
            <Route path="*" element={<Notfound />} /> 
         </Route>
         <Route
            path="/shop"
            element={
               <UserGuard>
                  <UserLayout />
               </UserGuard>
            }
         >
            <Route index element={<Navigate to="products" replace />} />
            <Route path="products" element={<UserProducts />} />
            <Route path="cart" element={<UserCart />} />
            <Route path="orders">
               <Route index element={<UserOrders />} />
               <Route path="order/:id" element={<UserOrderItem />} />
               <Route path="job-order" element={<UserJobOrder />} />
            </Route>
            <Route path="products" >
               <Route index element={<UserProducts />} />
               <Route path="product/:id" />
            </Route>
            <Route path="review">
               <Route index element={<UserReviews />} />
               <Route path="review/:id" />
            </Route>
            <Route path="settings" element={<UserSettings />} />
            <Route path="*" element={<Notfound />} />
         </Route>
      </Routes>
   </ThemeProvider>
);
}



export default App;
